package cn.itsource.pethome.basic.mapper;

import cn.itsource.pethome.basic.query.BaseQuery;

import java.util.List;

/**
 * 公共mapper
 * @param <T>
 */
public interface BaseMapper<T> {
    /**
     * 根据id查询domain信息
     * @param id
     * @return T
     */
    T findById(Long id);

    /**
     * 查询所有
     * @return
     * @param query
     */
    List<T> findPageByQuery(BaseQuery query);

    /**
     * 修改
     * @param T domain对象
     */
    void update(T T);

    /**
     * 添加
     * @param T domain对象
     */
    void add(T T);

    /**
     * 根据id删除一条信息
     * @param id
     */
    void delete(Long id);

    /**
     * 查询所有条数
     * @return
     */
    Long findCount(BaseQuery query);

    /**
     * 批量删除
     * @param list
     */
    void batchRemove(List<T> list);

}
