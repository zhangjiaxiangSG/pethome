package cn.itsource.pethome.basic.domain;

import lombok.Data;

import java.io.Serializable;

@Data
public class BaseDomain<T> implements Serializable{
    protected Long id;
}
