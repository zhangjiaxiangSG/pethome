package cn.itsource.pethome.basic.query;

/**
 * 高级查询
 */
public class BaseQuery {
    private Long cPage;
    private  Long pSize;
    public Long getBegin(){
        return (cPage - 1) * pSize;
    }
    public Long getcPage() {
        return cPage;
    }
    public void setcPage(Long cPage) {
        this.cPage = cPage;
    }
    public Long getpSize() {
        return pSize;
    }
    public void setpSize(Long pSize) {
        this.pSize = pSize;
    }
    @Override
    public String toString() {
        return "BaseQuery [cPage=" + cPage + ", pSize=" + pSize + "]";
    }

}
