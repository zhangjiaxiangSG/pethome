package cn.itsource.pethome.basic.config;

import cn.itsource.pethome.user.interceptor.LoginInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class ItsourceWebMvcConfigurer implements WebMvcConfigurer {
    @Autowired
    private LoginInterceptor loginInterceptor;
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(loginInterceptor)
                .addPathPatterns("/**")
                .excludePathPatterns("/login")
                .excludePathPatterns("/shop/settlement")
                .excludePathPatterns("/user/**")
                .excludePathPatterns("/user/activation/**")
                .excludePathPatterns("/verification/**")
                .excludePathPatterns("/wechat/**");
    }
}
