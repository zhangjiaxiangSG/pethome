package cn.itsource.pethome.basic.controller;

import cn.itsource.pethome.basic.service.IVerificationCodeService;
import cn.itsource.pethome.basic.util.AjaxResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * 验证码公用控制器
 */
@RestController
@RequestMapping("/verification")
public class VerificationCodeController {
    @Autowired
    IVerificationCodeService service;
    @RequestMapping("/sendRegisterMobileCode")
    public AjaxResult sendRegisterMobileCode(@RequestBody Map<String,String> param){
        try {
            //获取手机号码
            String phone = param.get("phone");
            service.sendRegisterMobileCode(phone);
            return new AjaxResult();
        } catch (Exception e) {
            e.printStackTrace();
            return new AjaxResult(false,e.getMessage());
        }
    }
    @RequestMapping("/sendBinderMobileCode")
    public AjaxResult sendBinderMobileCode(@RequestBody Map<String,String> param){
        try {
            //获取手机号码
            String phone = param.get("phone");
            service.sendBinderMobileCode(phone);
            return new AjaxResult();
        } catch (Exception e) {
            e.printStackTrace();
            return new AjaxResult(false,e.getMessage());
        }
    }
}
