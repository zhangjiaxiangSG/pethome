package cn.itsource.pethome.basic.controller;

import cn.itsource.pethome.basic.util.AjaxResult;
import cn.itsource.pethome.basic.util.FastDfsApiOpr;
import org.apache.commons.io.FilenameUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/fastdfs")
public class FastdfsController {

    @PostMapping("/upload")
    public AjaxResult upload(MultipartFile file){
        try {
            if(file==null){
               return new AjaxResult();
            }
            //获取文件名        1.jpg
            String filename = file.getOriginalFilename();
            //获取文件后缀名  jpg
            String extension = FilenameUtils.getExtension(filename);
            //上传附件(返回上传路径)
            String upload = FastDfsApiOpr.upload(file.getBytes(), extension);
            return new AjaxResult().setResultObj(upload);
        } catch (IOException e) {
            e.printStackTrace();
            return new AjaxResult(false, e.getMessage());
        }
    }
    @PatchMapping("/batchDelete")
    public AjaxResult batchDelete(@RequestBody List<Map<String,String>> list){
        try {
            //遍历
            for (Map<String,String> m : list) {
                //获取组名
               String  s = m.get("name");
                String group = s.substring(0,s.indexOf("/",1));
                String filename = s.substring(s.indexOf("/",1));
                 FastDfsApiOpr.delete(group,filename);

            }
            return new AjaxResult();
        } catch (Exception e) {
            e.printStackTrace();
                return new AjaxResult(false, e.getMessage());
        }
    }
}
