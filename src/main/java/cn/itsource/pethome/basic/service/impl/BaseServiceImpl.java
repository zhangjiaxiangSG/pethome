package cn.itsource.pethome.basic.service.impl;

import cn.itsource.pethome.basic.mapper.BaseMapper;
import cn.itsource.pethome.basic.query.BaseQuery;
import cn.itsource.pethome.basic.service.IBaseService;
import cn.itsource.pethome.basic.util.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
@Transactional(readOnly = true,propagation = Propagation.SUPPORTS)
public class BaseServiceImpl<T> implements IBaseService<T>{
    @Autowired
    BaseMapper<T> baseMapper;
    @Override
    public T findById(Long id) {
        return baseMapper.findById(id);
    }

    @Override
    public PageInfo<T> findPageByQuery(BaseQuery query) {
        //获取显示在页面的所有信息
        List<T> list = baseMapper.findPageByQuery(query);
        //获取总数
        Long total = baseMapper.findCount(query);
        if(total == 0l){//如果总数为0，直接返回空的信息
            return new PageInfo<T>();
        }
        return new PageInfo<T>(total,list);
    }

    @Override
    @Transactional
    public void add(T t) {
        baseMapper.add(t);
    }

    @Override
    @Transactional
    public void update(T t) {
        baseMapper.update(t);
    }

    @Override
    @Transactional
    public void save(T t) {
        baseMapper.add(t);
    }

    @Override
    @Transactional
    public void delete(Long id) {
        baseMapper.delete(id);
    }

    @Override
    @Transactional
    public void batchRemove(List<T> list) {
        baseMapper.batchRemove(list);
    }
}
