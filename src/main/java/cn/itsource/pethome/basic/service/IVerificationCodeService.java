package cn.itsource.pethome.basic.service;

import java.util.Map; /**
 * 验证码业务层
 */
public interface IVerificationCodeService {
    /**
     * 获取注册的验证码，
     * @param phone
     */
    void sendRegisterMobileCode(String phone);

    /**
     * 获取绑定微信的验证码
     * @param phone
     */
    void sendBinderMobileCode(String phone);
}
