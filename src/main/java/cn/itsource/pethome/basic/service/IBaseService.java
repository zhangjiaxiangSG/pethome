package cn.itsource.pethome.basic.service;

import cn.itsource.pethome.basic.domain.BaseDomain;
import cn.itsource.pethome.basic.query.BaseQuery;
import cn.itsource.pethome.basic.util.PageInfo;
import java.util.List;

public interface IBaseService<T> {
    /**
     * 根据id查询domain信息
     * @param id
     * @return
     */
    T findById(Long id);

    /**
     * 查询所有
     * @return
     * @param query
     */
    PageInfo<T> findPageByQuery(BaseQuery query);

    /**
     * 添加
     * @param t
     */
    void add(T t);

    /**
     * 修改
     * @param t
     */
    void update(T t);

    /**
     * 修改/添加
     * @param t
     */
    void save(T t);


    /**
     * 根据id删除一条domain信息
     * @param id
     */
    void delete(Long id);

    /**
     * 批量删除domain列表
     * @param list
     */
    void batchRemove(List<T> list);
}
