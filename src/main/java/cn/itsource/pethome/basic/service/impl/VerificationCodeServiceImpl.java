package cn.itsource.pethome.basic.service.impl;

import cn.itsource.pethome.basic.service.IVerificationCodeService;
import cn.itsource.pethome.basic.util.ContanstUtil;
import cn.itsource.pethome.basic.util.SendMsgUtil;
import cn.itsource.pethome.basic.util.StrUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.concurrent.TimeUnit;

/**
 * 手机验证码业务层实现
 */
@Service
public class VerificationCodeServiceImpl implements IVerificationCodeService {
    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * 发送注册的验证码
     * @param phone
     */
    @Override
    public void sendRegisterMobileCode(String phone) {
        sendMobileCode(phone,ContanstUtil.MOBILE_VERTIFICATION_CODE);

    }

    /**
     * 发送绑定的验证码
     * @param phone
     */
    @Override
    public void sendBinderMobileCode(String phone) {
        sendMobileCode(phone,ContanstUtil.BINDER_VERIFICATION_CODE);
    }

    /**
     * 发送手机验证码封装方法
     * @param phone
     * @param type
     */
    public void sendMobileCode(String phone,String type){
        //获取验证码
        String code = StrUtils.getComplexRandomString(4);
        //获取redis
        String codeValue = (String) redisTemplate.opsForValue().get(type+":"+phone);
        if(!StringUtils.isEmpty(codeValue)){//如果发送过验证码
            //如果距离上次发送验证码的时间小于1分钟，还是继续用一分钟之前的
            Long begin = Long.valueOf(codeValue.split(":")[0]);
            if(System.currentTimeMillis()-begin < 60*1000){
                //如果小于，抛出自定义异常
                throw new RuntimeException("不能重复获取验证码！");
            }
            code = codeValue.split(":")[1];
        }
        //设置redis，有效期为5分钟
        redisTemplate.opsForValue().set(type+":"+phone, System.currentTimeMillis()+":"+code,5, TimeUnit.MINUTES);
        String content = "你好，您的验证码为："+code+",请在5分钟内完成验证！";
        //SendMsgUtil.sendMsg(phone, content);
        System.out.println(content);
    }
}
