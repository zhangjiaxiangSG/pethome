package cn.itsource.pethome.basic.util;


import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.PostMethod;

import java.io.IOException;

public class SendMsgUtil {
    //本站用户名
    public static final String UID = "zhangjiaxiang";
    public static final String KEY = "d41d8cd98f00b204e980";

    /**
     * 发送邮件
     * @param phone  手机号码
     * @param content  短信内容
     */
    public static void sendMsg(String phone,String content){
        PostMethod post = null;
        try {
            //创建客户端
            HttpClient client = new HttpClient();
            //发送post请求
            post = new PostMethod("http://utf8.api.smschinese.cn");
            //添加请求头信息
            post.addRequestHeader("Content-Type","application/x-www-form-urlencoded;charset=utf8");//在头文件中设置转码
            //设置请求的基本信息
            NameValuePair[] data ={ new NameValuePair("Uid", UID),
                    new NameValuePair("Key", KEY),
                    new NameValuePair("smsMob",phone),
                    new NameValuePair("smsText",content)};
            //设置请求体
            post.setRequestBody(data);
            //开始调用
            client.executeMethod(post);

            //以下代码没什么用了，就是返回响应状态而已
            Header[] headers = post.getResponseHeaders();
            int statusCode = post.getStatusCode();
            System.out.println("statusCode:"+statusCode);
            for(Header h : headers)
            {
                System.out.println(h.toString());
            }
            String result = new String(post.getResponseBodyAsString().getBytes("gbk"));
            System.out.println(result); //打印返回消息状态

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if(post!=null){
                post.releaseConnection();
            }
        }
    }
}
