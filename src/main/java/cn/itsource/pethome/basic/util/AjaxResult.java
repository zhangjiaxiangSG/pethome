package cn.itsource.pethome.basic.util;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor//根据所有的字段产生全参数的构造方法
@NoArgsConstructor//创建无参数的构造方法
public class AjaxResult {
    private Boolean success = true;
    private String msg;
    //后端向前端响应的数据
    private Object resultObj;

    public AjaxResult(Boolean success,String msg){
        this.success = success;
        this.msg = msg;
    }

    public AjaxResult setResultObj(Object resultObj){
        this.resultObj = resultObj;
        return this;
    }



}
