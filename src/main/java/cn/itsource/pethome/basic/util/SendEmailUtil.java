package cn.itsource.pethome.basic.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;

/**
 * 发送邮件工具类
 */
@Component
public class SendEmailUtil {
    @Autowired
    private static JavaMailSender javaMailSender;
    public static void Send(String SendTo,String updateAddress){
        try {
            //创建复杂右键对象
            MimeMessage mimeMessage = javaMailSender.createMimeMessage();
            //创建复杂邮件工具类
            MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true, "utf-8");
            //设置邮件发送方
            mimeMessageHelper.setFrom("1299352517@qq.com");
            //设置邮件接收方
            mimeMessageHelper.setTo(SendTo);
            //设置邮件标题
            mimeMessageHelper.setSubject("邮箱注册");
            //设置邮件的文本
            mimeMessageHelper.setText("<h1>点击链接激活邮箱</h1>" +
                            "<a href='#' style='color:red;'>激活</a>"
                    ,true);
            //发送文件
            javaMailSender.send(mimeMessage);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }
}
