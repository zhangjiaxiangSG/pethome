package cn.itsource.pethome.basic.util;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PageInfo<T> {
    private Long total = 0l;
    private List list = new ArrayList<T>();
}
