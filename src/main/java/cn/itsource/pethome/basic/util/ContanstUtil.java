package cn.itsource.pethome.basic.util;

public class ContanstUtil {
    public static final String MOBILE_VERTIFICATION_CODE = "mobile_vertification_code";
    //绑定验证码
    public static final String BINDER_VERIFICATION_CODE = "binder_verification_code";
    //正常状态
    public static final Integer OK = 1;
    //待激活状态
    public static final Integer activated = 0;
    //第三方登录的账号
    public static final String APPID = "wxd853562a0548a7d0";
    public static final String SECRET = "4a5d5615f93f24bdba2ba8534642dbb6";
    //报账
    public static final Integer REIMBURSEMENT = 1;
    //打款
    public static final Integer PAYMENT = 2;
    //获取token对应的url地址
    public static final String TOKENURL = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=APPID&secret=SECRET&code=CODE&grant_type=authorization_code";

    //获取用户信息的url地址
    public static final String USERINFOURL = "https://api.weixin.qq.com/sns/userinfo?access_token=ACCESS_TOKEN&openid=OPENID";

    //回调域名
    public static final String REDIRECT_URI = "http://bugtracker.itsource.cn/wechat/callback";

    //申请授权码的url地址
    public static final String CODEURL = "https://open.weixin.qq.com/connect/qrconnect?appid=APPID&redirect_uri=REDIRECT_URI&response_type=code&scope=snsapi_login&state=STATE#wechat_redirect";
}
