package cn.itsource.pethome.product.mapper;

import cn.itsource.pethome.basic.mapper.BaseMapper;
import cn.itsource.pethome.product.domain.Product;
import cn.itsource.pethome.product.domain.ProductDetail;

import java.util.List;

public interface ProductDetailMapper extends BaseMapper<ProductDetail>{
    /**
     * 根据服务id删除服务明细
     * @param id
     */
    void deleteByProductid(Long id);

    /**
     * 批量删除明细，根据多个服务产品对象
     * @param list  服务产品列表
     */
    void batchRemoveByProducts(List<Product> list);

    /**
     * 根据产品id查询产品详情
     * @param id
     * @return
     */
    ProductDetail findoneByPID(Long id);
}
