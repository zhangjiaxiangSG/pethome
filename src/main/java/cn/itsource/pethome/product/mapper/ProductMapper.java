package cn.itsource.pethome.product.mapper;

import cn.itsource.pethome.basic.mapper.BaseMapper;
import cn.itsource.pethome.product.domain.Product;

import java.util.List;

public interface ProductMapper extends BaseMapper<Product>{
    /**
     * 批量上架
     * @param list
     */
    void onSale(List<Product> list);

    /**
     * 批量下架
     * @param list
     */
    void offSale(List<Product> list);
}
