package cn.itsource.pethome.product.query;

import cn.itsource.pethome.basic.query.BaseQuery;
import lombok.Data;

@Data
public class ProductQuery extends BaseQuery{
    private String name;
    private Integer state;
}
