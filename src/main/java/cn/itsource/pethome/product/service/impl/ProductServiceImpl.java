package cn.itsource.pethome.product.service.impl;

import cn.itsource.pethome.basic.query.BaseQuery;
import cn.itsource.pethome.basic.service.impl.BaseServiceImpl;
import cn.itsource.pethome.basic.util.PageInfo;
import cn.itsource.pethome.product.domain.Product;
import cn.itsource.pethome.product.domain.ProductDetail;
import cn.itsource.pethome.product.mapper.ProductDetailMapper;
import cn.itsource.pethome.product.mapper.ProductMapper;
import cn.itsource.pethome.product.service.IProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ProductServiceImpl extends BaseServiceImpl<Product> implements IProductService {
    @Autowired
    ProductMapper mapper;
    @Autowired
    ProductDetailMapper productDetailMapper;

    /**
     * 用于回显上传列表数据的
     * @param resources
     * @return
     */
    public List<Map<String,Object>> getFileList(String resources){
        ArrayList<Map<String,Object>> fileList = new ArrayList<>();
        String[] split = resources.split(",");
        String baseUrl = "http://115.159.217.249:8888";
        for (String s : split) {
            Map<String,Object> map = new HashMap<>();
            map.put("name",s);
            map.put("url",baseUrl+s);
            fileList.add(map);
        }
        return fileList;
    }
    @Override
    public PageInfo<Product> findPageByQuery(BaseQuery query) {
        //获取显示在页面的所有信息
        List<Product> list = mapper.findPageByQuery(query);
        for (Product product : list) {
            if(!StringUtils.isEmpty(product.getResources())) {//如果为空就不复制了
                product.setFileList(getFileList(product.getResources()));
            }
        }
        //获取总数
        Long total = mapper.findCount(query);
        if(total == 0l){//如果总数为0，直接返回空的信息
            return new PageInfo<Product>();
        }
        return new PageInfo<Product>(total,list);
    }
    @Override
    @Transactional
    public void save(Product product) {
        if(product.getId()==null){//如果是添加方法
            //先添加产品信息
            mapper.add(product);
            //再添加产品详情信息
            ProductDetail detail = product.getDetail();
            detail.setProduct(product);
            productDetailMapper.add(detail);
        }else{//如果是修改方法
            //先添加产品信息
            mapper.update(product);
            //再添加产品详情信息
            ProductDetail detail = product.getDetail();
            detail.setProduct(product);
            productDetailMapper.update(detail);
        }
    }

    @Override
    @Transactional
    public void delete(Long id) {
        //先删除产品详情
        productDetailMapper.deleteByProductid(id);
        //再删除本产品
        super.delete(id);
    }

    @Override
    @Transactional
    public void batchRemove(List<Product> list) {
        //先删除产品详情
        productDetailMapper.batchRemoveByProducts(list);
        //再删除本产品
        mapper.batchRemove(list);
    }

    /**
     * 上架
     * @param list
     */
    @Override
    @Transactional
    public void onSale(List<Product> list) {
        mapper.onSale(list);
    }
    /**
     * 下架
     * @param list
     */
    @Override
    @Transactional
    public void offSale(List<Product> list) {
        mapper.offSale(list);
    }
}
