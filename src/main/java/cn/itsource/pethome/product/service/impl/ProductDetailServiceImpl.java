package cn.itsource.pethome.product.service.impl;

import cn.itsource.pethome.product.domain.ProductDetail;
import cn.itsource.pethome.product.mapper.ProductDetailMapper;
import cn.itsource.pethome.product.service.IProductDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductDetailServiceImpl implements IProductDetailService {
    @Autowired
    ProductDetailMapper mapper;
    @Override
    public ProductDetail findoneByPID(Long id) {
        return mapper.findoneByPID(id);
    }
}
