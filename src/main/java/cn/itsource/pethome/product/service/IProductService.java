package cn.itsource.pethome.product.service;

import cn.itsource.pethome.basic.service.IBaseService;
import cn.itsource.pethome.basic.util.PageInfo;
import cn.itsource.pethome.product.domain.Product;
import cn.itsource.pethome.product.query.ProductQuery;

import java.util.List;

public interface IProductService extends IBaseService<Product>{
    //上架
    void onSale(List<Product> list);
    //下架
    void offSale(List<Product> list);
}
