package cn.itsource.pethome.product.service;

import cn.itsource.pethome.product.domain.ProductDetail;

public interface IProductDetailService {
    ProductDetail findoneByPID(Long id);
}
