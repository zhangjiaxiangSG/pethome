package cn.itsource.pethome.product.domain;

import cn.itsource.pethome.basic.domain.BaseDomain;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.math.BigDecimal;
import java.util.*;

@Data
public class Product extends BaseDomain{
    //姓名
    private String name;
    //图片地址们
    private String resources;
    //售价
    private BigDecimal saleprice;
    //下架时间,添加后默认是下架
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date offsaletime = new Date();
    //上架时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date onsaletime;
    //状态 0表示下架，1表示上架
    private Integer state = 0;
    //出售价格
    private BigDecimal costprice;
    //创建时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date createtime = new Date();
    //销售数量
    private Integer salecount = 0;
    private List<Map<String,Object>> fileList;
    private ProductDetail detail;

}
