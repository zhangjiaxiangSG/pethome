package cn.itsource.pethome.product.domain;

import cn.itsource.pethome.basic.domain.BaseDomain;
import lombok.Data;

@Data
public class ProductDetail extends BaseDomain{
    private Product product;
    //介绍
    private String intro;
    //预定须知
    private String orderNotice;
}
