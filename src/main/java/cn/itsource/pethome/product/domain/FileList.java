package cn.itsource.pethome.product.domain;

import lombok.Data;

@Data
public class FileList {
    private String name;
}
