package cn.itsource.pethome.product.controller;

import cn.itsource.pethome.basic.util.AjaxResult;
import cn.itsource.pethome.basic.util.PageInfo;
import cn.itsource.pethome.product.domain.Product;
import cn.itsource.pethome.product.query.ProductQuery;
import cn.itsource.pethome.product.service.IProductService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/product")
@Api(tags = "产品管理所有的接口",description = "产品接口详细描述")
public class ProductController {
    @Autowired
    IProductService service;
    @PatchMapping
    public PageInfo<Product> findPageByQuery(@RequestBody ProductQuery query){
        return service.findPageByQuery(query);
    }
    @PatchMapping("/findHomePageByQuery")
    public PageInfo<Product> findHomePageByQuery(@RequestBody ProductQuery query){
        return service.findPageByQuery(query);
    }
    @DeleteMapping("/{id}")
    public AjaxResult delete(@PathVariable("id") Long id){
        try {
            service.delete(id);
            return new AjaxResult();
        } catch (Exception e) {
            e.printStackTrace();
            return new AjaxResult(false,e.getMessage());
        }
    }
    @PostMapping
    public AjaxResult save(@RequestBody Product product){
        try {
            service.save(product);
            return new AjaxResult();
        } catch (Exception e) {
            e.printStackTrace();
            return new AjaxResult(false,e.getMessage());
        }
    }
    @PatchMapping("/batchRemove")
    public AjaxResult batchRemove(@RequestBody List<Product> list){
        try {
            service.batchRemove(list);
            return new AjaxResult();
        } catch (Exception e) {
            e.printStackTrace();
            return new AjaxResult(false,e.getMessage());
        }
    }
    @PatchMapping("/onSale")
    public AjaxResult onSale(@RequestBody List<Product> list){
        try {
            service.onSale(list);
            return new AjaxResult();
        } catch (Exception e) {
            e.printStackTrace();
            return new AjaxResult(false,e.getMessage());
        }
    }
    @PatchMapping("/offSale")
    public AjaxResult offSale(@RequestBody List<Product> list){
        try {
            service.offSale(list);
            return new AjaxResult();
        } catch (Exception e) {
            e.printStackTrace();
            return new AjaxResult(false,e.getMessage());
        }
    }
}
