package cn.itsource.pethome.product.controller;

import cn.itsource.pethome.basic.util.AjaxResult;
import cn.itsource.pethome.product.domain.ProductDetail;
import cn.itsource.pethome.product.service.IProductDetailService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/productDetail")
@Api(tags = "产品管理详细所有的接口",description = "产品详细接口详细描述")
public class ProductDetailController {
    @Autowired
    IProductDetailService service;
    @GetMapping("/{id}")
    public AjaxResult findoneByPID(@PathVariable("id") Long id){
        try {
            ProductDetail productDetail = service.findoneByPID(id);
            return new AjaxResult().setResultObj(productDetail);
        } catch (Exception e) {
            e.printStackTrace();
            return new AjaxResult(false,e.getMessage());
        }
    }

}
