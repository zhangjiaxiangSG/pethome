package cn.itsource.pethome.user.controller;

import cn.itsource.pethome.basic.util.AjaxResult;
import cn.itsource.pethome.basic.util.ContanstUtil;
import cn.itsource.pethome.user.domain.dto.UserDto;
import cn.itsource.pethome.user.service.IWechatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@Controller
@RequestMapping("/wechat")
public class WechatController {
    @Autowired
    IWechatService wechatService;
    /**
     * 申请授权码
     *
     * 申请授权码的方法
     * @return
     */
    @GetMapping("/toRequestCode")
    public String toRequestCode(){
        return "redirect:"+ ContanstUtil.CODEURL.replace("APPID", ContanstUtil.APPID)
                .replace("REDIRECT_URI", ContanstUtil.REDIRECT_URI);
    }

    /**
     * 回调函数，获取到请求码的回调函数，就是扫描微信之后，要跳转的地方,请求吗用参数接收
     * @return
     */
    @RequestMapping("/callback")
    public String callback(String code){
        return "redirect:http://localhost:8082/callback.html?code=" + code;
    }

    /**
     * 通过授权码获取资源
     * @param param
     * @return
     */
    @PostMapping("/handleCallBack")
    @ResponseBody
    public AjaxResult handleCallBack(@RequestBody Map<String,Object> param){
        try {
            String code = (String) param.get("code");
            //通过授权码向服务器响应,得到资源
            Map<String,Object> map = wechatService.handleCallBack(code);
            return new AjaxResult().setResultObj(map);
        } catch (Exception e) {
            e.printStackTrace();
            return new AjaxResult(false,e.getMessage());
        }
    }
    @PostMapping("/binder")
    @ResponseBody
    public AjaxResult binder(@RequestBody UserDto userDto){
        try {
            //绑定用户
            Map<String,Object> map = wechatService.binder(userDto);
            return new AjaxResult().setResultObj(map);
        } catch (Exception e) {
            e.printStackTrace();
            return new AjaxResult(false,e.getMessage());
        }
    }
}
