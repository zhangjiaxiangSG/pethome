package cn.itsource.pethome.user.controller;

import cn.itsource.pethome.basic.util.AjaxResult;
import cn.itsource.pethome.user.domain.dto.UserDto;
import cn.itsource.pethome.user.service.ILoginInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LoginController {
    @Autowired
    ILoginInfoService loginInfoService;
    @PostMapping("/login")
    public AjaxResult login(@RequestBody UserDto userDto){
        try {
            return loginInfoService.login(userDto);
        } catch (Exception e) {
            e.printStackTrace();
            return new AjaxResult(false,e.getMessage());
        }
    }
}
