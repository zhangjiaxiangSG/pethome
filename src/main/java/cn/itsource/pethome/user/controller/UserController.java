package cn.itsource.pethome.user.controller;

import cn.itsource.pethome.basic.util.AjaxResult;
import cn.itsource.pethome.user.domain.dto.UserDto;
import cn.itsource.pethome.user.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/user")
@RestController
public class UserController {
    @Autowired
    private IUserService userService;
    @PostMapping("/register")
    public AjaxResult register(@RequestBody UserDto userDto){
        try {
            userService.register(userDto);
            return new AjaxResult();
        } catch (Exception e) {
            e.printStackTrace();
            return new AjaxResult(false,e.getMessage());
        }
    }

    @PostMapping("/EmailRegister")
    public AjaxResult EmailRegister(@RequestBody UserDto userDto){
        try {
            userService.EmailRegister(userDto);
            return new AjaxResult();
        } catch (Exception e) {
            e.printStackTrace();
            return new AjaxResult(false,e.getMessage());
        }
    }

    /**
     * 激活信息
     * @param id
     * @return
     */
    @GetMapping("/activation/{id}")
    public AjaxResult activation(@PathVariable("id") Long id){
        try {
            //激活
            userService.activation(id);
            return new AjaxResult();
        } catch (Exception e) {
            e.printStackTrace();
            return new AjaxResult(false,e.getMessage());
        }
    }
}
