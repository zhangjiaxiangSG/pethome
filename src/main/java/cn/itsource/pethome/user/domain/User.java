package cn.itsource.pethome.user.domain;

import cn.itsource.pethome.basic.domain.BaseDomain;
import io.swagger.models.auth.In;
import lombok.Data;

import java.util.Date;

@Data
public class User extends BaseDomain<User>{
    //用户名
    private String username;

    private String email;

    private String phone;
    //盐值，用于密码的的加密
    private String salt;
    //密码
    private String password;
    //状态 0表示待激活，1表示正常
    private Integer state;
    private Integer age;
    //创建时间
    private Date createtime = new Date();
    //头像
    private String headImg;
    //登录信息
    private LoginInfo loginInfo;
}
