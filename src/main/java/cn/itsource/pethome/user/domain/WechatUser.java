package cn.itsource.pethome.user.domain;

import cn.itsource.pethome.basic.domain.BaseDomain;
import lombok.Data;

/**
 * 微信用户实体对象
 */
@Data
public class WechatUser extends BaseDomain {
    //微信用户唯一标志
    private String openid;
    //昵称
    private String nickname;
    //性别
    private Boolean sex;
    //地址
    private String address;
    //头像
    private String headimgurl;
    //授权之后唯一标志
    private String unionid;
    //关联登录信息
    private LoginInfo loginInfo;
}
