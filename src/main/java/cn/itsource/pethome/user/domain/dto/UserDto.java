package cn.itsource.pethome.user.domain.dto;

import lombok.Data;

/**
 *  domain, pojo,entity   专门存储数据库的数据
 *  vo  dto : 一般就是临时对象(临时封装前端传递的数据==》domain和前端传递的数据对应不上)
 */
@Data
public class UserDto {
    //用户名
    private String username;
    //手机号
    private String phone;
    //邮箱
    private String email;
    //验证码
    private String code;
    //密码
    private String password;
    //确认密码
    private String configPassword;
    // true:前端用户   false:商家/平台
    private Boolean type;
    //openid，微信用户的唯一标志
    private String openid;
}
