package cn.itsource.pethome.user.domain;

import cn.itsource.pethome.basic.domain.BaseDomain;
import lombok.Data;

/**
 * 登录信息表
 */
@Data
public class LoginInfo extends BaseDomain{
    //用户名
    private String username;
    //电话
    private String phone;
    private String email;
    //颜值
    private String salt;
    //密码
    private String password;
    //类型 0表示平台人员，1表示普通用户
    private Boolean type;
    //禁用
    private Boolean disable = true;
}
