package cn.itsource.pethome.user.service;

import cn.itsource.pethome.basic.service.IBaseService;
import cn.itsource.pethome.basic.util.AjaxResult;
import cn.itsource.pethome.user.domain.LoginInfo;
import cn.itsource.pethome.user.domain.dto.UserDto;

public interface ILoginInfoService extends IBaseService<LoginInfo>{
    /**
     * 登录业务层
     */
    AjaxResult login(UserDto userDto);
}
