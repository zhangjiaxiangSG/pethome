package cn.itsource.pethome.user.service;

import cn.itsource.pethome.user.domain.dto.UserDto;

import java.util.Map;

public interface IWechatService {
    /**
     * 通过授权码处理回调函数
     * @param code
     * @return
     */
     Map<String,Object> handleCallBack(String code);

    /**
     * 绑定微信
     * @param userDto
     * @return
     */
    Map<String,Object> binder(UserDto userDto);
}
