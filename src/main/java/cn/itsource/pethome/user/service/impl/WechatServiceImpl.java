package cn.itsource.pethome.user.service.impl;

import cn.itsource.pethome.basic.util.*;
import cn.itsource.pethome.user.domain.LoginInfo;
import cn.itsource.pethome.user.domain.User;
import cn.itsource.pethome.user.domain.WechatUser;
import cn.itsource.pethome.user.domain.dto.UserDto;
import cn.itsource.pethome.user.mapper.LoginInfoMapper;
import cn.itsource.pethome.user.mapper.UserMapper;
import cn.itsource.pethome.user.mapper.WechatMapper;
import cn.itsource.pethome.user.service.IWechatService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.databind.util.BeanUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import springfox.documentation.spring.web.json.Json;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Service
public class WechatServiceImpl implements IWechatService {
    @Autowired
    WechatMapper wechatMapper;
    @Autowired
    UserMapper userMapper;
    @Autowired
    LoginInfoMapper loginInfoMapper;
    @Autowired
    private RedisTemplate redisTemplate;
    @Override
    public Map<String, Object> handleCallBack(String code) {
        //通过授权码获取令牌
        //获取tokenurl地址
        String tokenUrl = ContanstUtil.TOKENURL.replace("APPID", ContanstUtil.APPID)
                .replace("SECRET", ContanstUtil.SECRET)
                .replace("CODE", code);
        //使用java程序发送get请求，它就会返回json格式
        String tokenJsonStr = HttpClientUtils.httpGet(tokenUrl);
        //字符串解析成对象
        JSONObject jsonObject = JSONObject.parseObject(tokenJsonStr);
        //获取openid
        String openid = (String) jsonObject.get("openid");
        //获取access_token令牌
        String access_token = (String) jsonObject.get("access_token");
        //通过token获取资源
        String userInfoUrl = ContanstUtil.USERINFOURL.replace("ACCESS_TOKEN", access_token)
                .replace("OPENID", openid);
        //根据令牌和openid获取微信用户基本信息(json字符串)
        String userInfoJsonStr = HttpClientUtils.httpGet(userInfoUrl);
        //解析用户信息字符串为json对象
        JSONObject WechatUserInfo = JSON.parseObject(userInfoJsonStr);
        //根据openid查询是否在表里已经有此对象了
        WechatUser wechatUser = wechatMapper.findByopenid(openid);
        Map<String,Object> map = new HashMap<>();
        //如果没有对象，说明是第一次微信登录，就添加在表里
        if(wechatUser == null){
            WechatUser WUser = new WechatUser();
            WUser.setOpenid(openid);
            WUser.setNickname(WechatUserInfo.getString("nickname"));
            WUser.setSex(WechatUserInfo.getBoolean("sex"));
            //获取地址
            String address = WechatUserInfo.getString("province")+" "+WechatUserInfo.getString("city");
            WUser.setAddress(address);
            WUser.setHeadimgurl(WechatUserInfo.getString("headimgurl"));
            WUser.setUnionid(WechatUserInfo.getString("unionid"));
            //把新信息添加到微信表里面
            wechatMapper.add(WUser);
            //添加完信息了，但是没有登录id，所以要进行绑定
            //传入openid给前台，前台接收来请求绑定后台进行绑定
            map.put("openid",openid);
            return map;
        }else{//如果有对象，就走这一步
            //先判断对象里面是否已经绑定了登录对象
            LoginInfo loginInfo = wechatUser.getLoginInfo();
            //如果登录对象不存在，就绑定登录对象
            if(loginInfo == null){
                //绑定登陆对象
                //传入openid给前台，前台接收来请求绑定后台进行绑定
                map.put("openid",openid);
                return map;
            }else{
                //跳转到登录界面，设置session
                //随机创建token
                String token = UUID.randomUUID().toString();
                //直接登录
                redisTemplate.opsForValue().set(token, loginInfo,30, TimeUnit.MINUTES);
                map.put("token", token);
                map.put("loginUser", loginInfo);
                return map;
            }
        }
    }

    @Override
    public Map<String, Object> binder(UserDto userDto) {
        //检查userdto的正确性
        checkUserDto(userDto);
        //手机号码是否在数据库中已存在
        LoginInfo loginInfo = loginInfoMapper.findByUser(userDto);
        Map<String, Object> map = new HashMap<>();
        if(loginInfo==null){//如果这个手机号没有被注册过
            //userdto对象转换成logininfo对象
            loginInfo = user2logininfo(userDto);
            //保存logininfo对象
            loginInfoMapper.add(loginInfo);
            //logininfo对象转换成user对象
           User user = logininfo2user(loginInfo);
           //保存user对象
            userMapper.add(user);
        }
        //修改wechat对象，通过openid和loginid
        wechatMapper.binder(userDto.getOpenid(),loginInfo.getId());
        //生成uuid
        String token = UUID.randomUUID().toString();
        //设置redis缓存模拟session
        redisTemplate.opsForValue().set(token,loginInfo,30, TimeUnit.MINUTES);
        map.put("token", token);
        map.put("loginUser", loginInfo);
        //返回对
        return map;
    }

    private User logininfo2user(LoginInfo loginInfo) {
        User user = new User();
        BeanUtils.copyProperties(loginInfo, user);
        user.setState(ContanstUtil.OK);
        user.setLoginInfo(loginInfo);
        return user;

    }

    private LoginInfo user2logininfo(UserDto userDto) {
        LoginInfo loginInfo = new LoginInfo();
        loginInfo.setPhone(userDto.getPhone());
        loginInfo.setUsername(userDto.getPhone());
        loginInfo.setType(userDto.getType());
        loginInfo.setSalt(StrUtils.getComplexRandomString(10));
        loginInfo.setPassword(MD5Utils.encrypByMd5(loginInfo.getPhone()+loginInfo.getSalt()));
        return loginInfo;

    }

    private void checkUserDto(UserDto userDto) {
        if(userDto.getType()==null || StringUtils.isEmpty(userDto.getPhone())
                || StringUtils.isEmpty(userDto.getCode())){
            throw new RuntimeException("请检查完整性！");
        }
        String codevalue = (String)redisTemplate.opsForValue().get(ContanstUtil.BINDER_VERIFICATION_CODE+":"+userDto.getPhone());
        if(StringUtils.isEmpty(codevalue)){
            throw new RuntimeException("验证码不存在！");
        }
        String s = codevalue.split(":")[1];
        if(!s.toUpperCase().equals(userDto.getCode().toUpperCase())){
            throw new RuntimeException("验证码错误！");
        }

    }
}
