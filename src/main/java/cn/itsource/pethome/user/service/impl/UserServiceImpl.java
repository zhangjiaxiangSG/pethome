package cn.itsource.pethome.user.service.impl;

import cn.itsource.pethome.basic.query.BaseQuery;
import cn.itsource.pethome.basic.service.impl.BaseServiceImpl;
import cn.itsource.pethome.basic.util.*;
import cn.itsource.pethome.user.domain.LoginInfo;
import cn.itsource.pethome.user.domain.User;
import cn.itsource.pethome.user.domain.dto.UserDto;
import cn.itsource.pethome.user.mapper.LoginInfoMapper;
import cn.itsource.pethome.user.mapper.UserMapper;
import cn.itsource.pethome.user.service.IUserService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.List;
@Service
@Transactional
public class UserServiceImpl extends BaseServiceImpl<User> implements IUserService {
    @Autowired
    UserMapper userMapper;
    @Autowired
    LoginInfoMapper loginInfoMapper;
    @Autowired
    RedisTemplate redisTemplate;
    @Autowired
    private JavaMailSender javaMailSender;
    public void Send(String SendTo,String updateAddress){
        try {
            //创建复杂右键对象
            MimeMessage mimeMessage = javaMailSender.createMimeMessage();
            //创建复杂邮件工具类
            MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true, "utf-8");
            //设置邮件发送方
            mimeMessageHelper.setFrom("1299352517@qq.com");
            //设置邮件接收方
            mimeMessageHelper.setTo(SendTo);
            //设置邮件标题
            mimeMessageHelper.setSubject("邮箱注册");
            //设置邮件的文本
            mimeMessageHelper.setText("<h1>点击链接激活邮箱</h1>" +
                            "<a href='" +updateAddress+
                            "' style='color:red;'>激活</a>"
                    ,true);
            //发送文件
            javaMailSender.send(mimeMessage);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    /**
     * 手机号注册
     * @param userDto
     */
    @Override
    public void register(UserDto userDto) {
        //校验数据是否正确
        checkUserDto(userDto);
        //将userdto转换成logininfo
        LoginInfo loginInfo = userdto2logininfo(userDto);
        //添加logininfo对象在表里面
        loginInfoMapper.add(loginInfo);
        //将logininfo转换成user
        User user = logininfo2User(loginInfo);
        //添加user对象在user表里面
        userMapper.add(user);
    }

    /**
     * 邮箱注册
     * @param userDto
     */
    @Override
    public void EmailRegister(UserDto userDto) {
        //校验数据是否正确
        checkEmailRegisterUserDto(userDto);
        //将userdto转换成logininfo
        LoginInfo loginInfo = userdto2logininfo(userDto);
        //添加logininfo对象在表里面
        loginInfoMapper.add(loginInfo);
        //将logininfo转换成user
        User user = logininfo2User(loginInfo);
        //添加user对象在user表里面
        userMapper.add(user);
        //发送邮箱，邮箱进行激活
        String updAddress = "http://localhost/user/activation?id"+user.getId();
        Send(user.getEmail(),updAddress);
    }

    @Override
    public void activation(Long id) {
        //根据id跟新user表里面的state
        userMapper.updateByid(id,ContanstUtil.OK);
        //在根据id跟新logininfo表里面的disable
        User byId = userMapper.findById(id);
        LoginInfo loginInfo = byId.getLoginInfo();
        loginInfo.setDisable(true);
        loginInfoMapper.update(loginInfo);
    }

    /**
     * 检查邮箱验证传过来的参数正确性
     * @param userDto
     */
    private void checkEmailRegisterUserDto(UserDto userDto) {
        //判断传入的参数是否为空
        if(StringUtils.isEmpty(userDto.getEmail())
                ||StringUtils.isEmpty(userDto.getPassword())
                ||StringUtils.isEmpty(userDto.getConfigPassword())){
            throw new RuntimeException("请把信息填写完整！");
        };
        //判断手机号是否正确
        //查询是否有手机号
        User byEmail = userMapper.findByEmail(userDto.getEmail());
        if(byEmail != null){
            throw new RuntimeException("邮箱已被注册！");
        }
        //判断两次密码是否一致
        if(!userDto.getPassword().equals(userDto.getConfigPassword())){
            throw new RuntimeException("两次密码不一致！");
        }
    }

    /**
     * 登录对象转换成user对象
     * @param loginInfo
     * @return
     */
    private User logininfo2User(LoginInfo loginInfo) {
        //赋值属性
        User user = new User();
        //只要属性就能拷贝
        BeanUtils.copyProperties(loginInfo, user);
        //0: 待激活    1:正常
        if(!StringUtils.isEmpty(loginInfo.getEmail())){
            user.setState(ContanstUtil.activated);//设置为待激活
        }else{
            user.setState(ContanstUtil.OK);
        }
        user.setLoginInfo(loginInfo);
        return user;
    }

    /**
     * 把userdto对象转换成logininfo对象
     * @param userDto
     * @return
     */
    private LoginInfo userdto2logininfo(UserDto userDto) {
        LoginInfo loginInfo = new LoginInfo();


        if(!StringUtils.isEmpty(userDto.getPhone())) {//如果是手机注册
            //设置用户名
            loginInfo.setUsername(userDto.getPhone());
            //设置电话
            loginInfo.setPhone(userDto.getPhone());

        }else if(!StringUtils.isEmpty(userDto.getEmail())){//如果是邮箱注册
            //设置用户名
            loginInfo.setUsername(userDto.getEmail());
            //设置电话
            loginInfo.setEmail(userDto.getEmail());
            loginInfo.setDisable(false);//设置为禁用，激活后才可以使用
        }
        //设置盐值
        loginInfo.setSalt(StrUtils.getComplexRandomString(10));
        //设置密码
        loginInfo.setPassword(MD5Utils.encrypByMd5(userDto.getPassword() + loginInfo.getSalt()));
        //设置为前端人员登录
        loginInfo.setType(true);
        return loginInfo;
    }

    /**
     * 判断传入注册的对象正确性
     * @param userDto
     */
    private void checkUserDto(UserDto userDto) {
        //判断传入的参数是否为空
        if(StringUtils.isEmpty(userDto.getPhone()) || StringUtils.isEmpty(userDto.getCode())
                ||StringUtils.isEmpty(userDto.getPassword())
                ||StringUtils.isEmpty(userDto.getConfigPassword())){
            throw new RuntimeException("请把信息填写完整！");
        };
        //判断手机号是否正确
        //查询是否有手机号
        User byPhone = userMapper.findByPhone(userDto.getPhone());
        if(byPhone != null){
            throw new RuntimeException("电话号码已被注册！");
        }
        //判断验证码是否存在
        //获取验证码
        String codeValue = (String)redisTemplate.opsForValue().get(ContanstUtil.MOBILE_VERTIFICATION_CODE + ":" + userDto.getPhone());
        if(StringUtils.isEmpty(codeValue)){
            throw new RuntimeException("验证码已过期！");
        }
        //校验验证码是否正确
        String code = codeValue.split(":")[1];
        if(!code.toUpperCase().equals(userDto.getCode().toUpperCase())){
            throw new RuntimeException("验证码不正确，请重新输入！");
        }
        //判断两次密码是否一致
        if(!userDto.getPassword().equals(userDto.getConfigPassword())){
            throw new RuntimeException("两次密码不一致！");
        }
    }

}
