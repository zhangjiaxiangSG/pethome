package cn.itsource.pethome.user.service.impl;

import cn.itsource.pethome.basic.service.impl.BaseServiceImpl;
import cn.itsource.pethome.basic.util.AjaxResult;
import cn.itsource.pethome.basic.util.MD5Utils;
import cn.itsource.pethome.user.domain.LoginInfo;
import cn.itsource.pethome.user.domain.dto.UserDto;
import cn.itsource.pethome.user.mapper.LoginInfoMapper;
import cn.itsource.pethome.user.service.ILoginInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
@Service
public class LoginInfoServiceImpl extends BaseServiceImpl<LoginInfo> implements ILoginInfoService {
    @Autowired
    LoginInfoMapper loginInfoMapper;
    @Autowired
    RedisTemplate redisTemplate;
    @Override
    public AjaxResult login(UserDto userDto) {
            //验证对象是否正确
            LoginInfo loginInfo = checkuserDto(userDto);
            //模仿session，用redis复刻一个session
            //设置jsessionid
            String token = UUID.randomUUID().toString();
            //设置有效期为30分钟
            redisTemplate.opsForValue().set(token,loginInfo,30, TimeUnit.MINUTES);
            Map<String,Object> map = new HashMap<>();
            map.put("token", token);
            map.put("loginInfo", loginInfo);
            //返回session对象
            return new AjaxResult().setResultObj(map);
    }

    /**
     * 验证前端传的值正确性
     * @param userDto
     */
    private LoginInfo checkuserDto(UserDto userDto) {
        //判断是否为空
        if(StringUtils.isEmpty(userDto.getUsername())
                || StringUtils.isEmpty(userDto.getPassword())
                || userDto.getType()==null){
            throw new RuntimeException("请输入用户名或者密码！");
        }
        //根据用户名查询是否有这个用户
        LoginInfo loginInfo = loginInfoMapper.findByUser(userDto);
        //如果没有，抛出异常
        if(loginInfo==null){
            throw new RuntimeException("用户名错误！");
        }
        //判断密码是否正确
        //先加密前端传入的密码
        String pwdMd5 = MD5Utils.encrypByMd5(userDto.getPassword() + loginInfo.getSalt());
        if (!pwdMd5.equals(loginInfo.getPassword())){
            //如果不等于。抛出异常
            throw new RuntimeException("密码错误！");
        }
        //判断账号是否禁用了
        if(!loginInfo.getDisable()){
            throw new RuntimeException("该账号已被禁用，请联系管理人员!!");
        }
        return loginInfo;
    }

}
