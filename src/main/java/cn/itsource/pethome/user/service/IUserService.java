package cn.itsource.pethome.user.service;

import cn.itsource.pethome.basic.service.IBaseService;
import cn.itsource.pethome.user.domain.User;
import cn.itsource.pethome.user.domain.dto.UserDto;

public interface IUserService extends IBaseService<User>{
    void register(UserDto userDto);

    /**
     * 邮箱注册
     * @param userDto
     */
    void EmailRegister(UserDto userDto);

    /**
     * 激活信息
     * @param id
     */
    void activation(Long id);
}
