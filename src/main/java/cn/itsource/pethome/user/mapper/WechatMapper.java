package cn.itsource.pethome.user.mapper;

import cn.itsource.pethome.user.domain.WechatUser;
import org.apache.ibatis.annotations.Param;

public interface WechatMapper {
    /**
     * 根据openid获取微信表对象
     * @param openid
     */
    WechatUser findByopenid(String openid);

    /**
     * 添加微信信息
     * @param wechatUser
     */
    void add(WechatUser wechatUser);

    /**
     * 修改wecahtuser里面的loginid值
     * @param openid
     * @param id
     */
    void binder(@Param("openid") String openid, @Param("logininfo_id") Long id);
}
