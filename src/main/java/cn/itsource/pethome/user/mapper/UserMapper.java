package cn.itsource.pethome.user.mapper;

import cn.itsource.pethome.basic.mapper.BaseMapper;
import cn.itsource.pethome.user.domain.User;
import org.apache.ibatis.annotations.Param;

public interface UserMapper extends BaseMapper<User>{
    /**
     * 根据电话号码查寻user对象
     * @param phone
     * @return
     */
    User findByPhone(String phone);

    /**
     * 查询邮箱是否存在
     * @param email
     * @return
     */
    User findByEmail(String email);

    /**
     * 跟新激活状态
     * @param id
     * @param state
     */
    void updateByid(@Param("id") Long id, @Param("state") Integer state);

    /**
     * 通过登陆对象的id查询useer对象
     * @return
     */
    User findOneByLoginInfoId(Long id);
}
