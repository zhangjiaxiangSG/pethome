package cn.itsource.pethome.user.mapper;

import cn.itsource.pethome.basic.mapper.BaseMapper;
import cn.itsource.pethome.user.domain.LoginInfo;
import cn.itsource.pethome.user.domain.dto.UserDto;
import com.fasterxml.jackson.databind.ser.Serializers;

public interface LoginInfoMapper extends BaseMapper<LoginInfo>{
    //根据USEDTO查询是否有此对象
    LoginInfo findByUser(UserDto userDto);
}
