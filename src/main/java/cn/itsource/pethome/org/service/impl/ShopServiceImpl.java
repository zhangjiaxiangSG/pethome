package cn.itsource.pethome.org.service.impl;

import cn.itsource.pethome.basic.service.impl.BaseServiceImpl;
import cn.itsource.pethome.basic.util.AjaxResult;
import cn.itsource.pethome.org.domain.Department;
import cn.itsource.pethome.org.domain.Employee;
import cn.itsource.pethome.org.domain.Shop;
import cn.itsource.pethome.org.mapper.DepartmentMapper;
import cn.itsource.pethome.org.mapper.EmployeeMapper;
import cn.itsource.pethome.org.mapper.ShopMapper;
import cn.itsource.pethome.org.service.IDepartmentService;
import cn.itsource.pethome.org.service.IEmployeeService;
import cn.itsource.pethome.org.service.IShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ShopServiceImpl extends BaseServiceImpl<Shop> implements IShopService {
   @Autowired
    IEmployeeService employeeService;
   @Autowired
   ShopMapper shopMapper;

    @Override
    @Transactional
    public void settlement(Shop shop) {
        Employee admin = shop.getAdmin();
        employeeService.add(admin);
        shopMapper.add(shop);
        admin.setShop(shop);
        employeeService.update(admin);
    }
}
