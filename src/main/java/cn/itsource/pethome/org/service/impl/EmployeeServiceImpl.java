package cn.itsource.pethome.org.service.impl;

import cn.itsource.pethome.basic.service.impl.BaseServiceImpl;
import cn.itsource.pethome.basic.util.MD5Utils;
import cn.itsource.pethome.basic.util.StrUtils;
import cn.itsource.pethome.org.domain.Employee;
import cn.itsource.pethome.org.mapper.EmployeeMapper;
import cn.itsource.pethome.org.service.IEmployeeService;
import cn.itsource.pethome.user.domain.LoginInfo;
import cn.itsource.pethome.user.mapper.LoginInfoMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class EmployeeServiceImpl extends BaseServiceImpl<Employee> implements IEmployeeService{
    @Autowired
    EmployeeMapper mapper;
    @Autowired
    LoginInfoMapper loginInfoMapper;
    @Override
    public List<Employee> findManagers() {
        return mapper.findManagers();
    }
    @Override
    public void add(Employee employee){
        //设置颜值
        employee.setSalt(StrUtils.getComplexRandomString(10));
        employee.setPassword(MD5Utils.encrypByMd5(employee.getPassword()+employee.getSalt()));
        //设置密码加密
        //员工对象装换成logininfo对象
        LoginInfo loginInfo = employee2logininfo(employee);

        //添加信息到emloyee表里面
        loginInfoMapper.add(loginInfo);
        //再把信息赋值到logininfo里面
        employee.setLogin(loginInfo);
        mapper.add(employee);
    }

    /**
     * 员工对象转换成登录对象
     * @param employee
     * @return
     */
    private LoginInfo employee2logininfo(Employee employee) {
        LoginInfo loginInfo = new LoginInfo();
        BeanUtils.copyProperties(employee,loginInfo);
        //设置为后端人员
        loginInfo.setType(false);
        return loginInfo;
    }
}
