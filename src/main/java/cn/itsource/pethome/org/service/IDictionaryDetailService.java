package cn.itsource.pethome.org.service;

import cn.itsource.pethome.basic.service.IBaseService;
import cn.itsource.pethome.org.domain.Department;
import cn.itsource.pethome.org.domain.DictionaryDetail;

import java.util.List;

/**
 * 数据字典明细业务层
 */
public interface IDictionaryDetailService extends IBaseService<DictionaryDetail> {
    //根据类型id删除所有信息
    void deleteBytid(Long tid);
}