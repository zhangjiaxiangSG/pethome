package cn.itsource.pethome.org.service.impl;

import cn.itsource.pethome.basic.query.BaseQuery;
import cn.itsource.pethome.basic.service.impl.BaseServiceImpl;
import cn.itsource.pethome.basic.util.PageInfo;
import cn.itsource.pethome.org.domain.Department;
import cn.itsource.pethome.org.domain.DictionaryDetail;
import cn.itsource.pethome.org.mapper.DepartmentMapper;
import cn.itsource.pethome.org.mapper.DictionaryDetailMapper;
import cn.itsource.pethome.org.service.IDictionaryDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 数据字典明细实现类
 */
@Service
public class DictionaryDetailServiceImpl extends BaseServiceImpl<DictionaryDetail> implements IDictionaryDetailService {
    @Autowired
    DictionaryDetailMapper mapper;
    @Override
    public void save(DictionaryDetail detail){
        if(detail.getId()==null){
            mapper.add(detail);
        }else{
            mapper.update(detail);
        }
    }

    @Override
    @Transactional
    public void deleteBytid(Long tid) {
        mapper.deleteBytid(tid);
    }
}

