package cn.itsource.pethome.org.service.impl;

import cn.itsource.pethome.basic.service.impl.BaseServiceImpl;
import cn.itsource.pethome.org.domain.Department;
import cn.itsource.pethome.org.mapper.DepartmentMapper;
import cn.itsource.pethome.org.service.IDepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DepartmentServiceImpl extends BaseServiceImpl<Department> implements IDepartmentService {
    @Autowired
    DepartmentMapper mapper;
    public void findeptsTree(List<Department> departments,Department dept){
        if(departments.size()!=0){
            for (Department department : departments) {
                //通过父id获取子集部门
                List<Department> list = mapper.findByPid(department.getId());
                //设置不能选中当前二级部门
                if(dept.getId()!=null){
                    if(department.getId().equals(dept.getId())){
                        department.setDisabled(true);
                    }
                }
                if(list.size()!=0){
                    //设置子部门
                    department.setChildren(list);
                    //递归调用
                    findeptsTree(list,dept);
                }
            }
        }
    }
    @Override
    public List<Department> findDeptsTree(Department dept) {
        List<Department> oneDepts = mapper.findOneDepts();
        if(oneDepts.size()!=0){
            findeptsTree(oneDepts,dept);
        }
        //设置不能选中当前一级部门
        for (Department oneDept : oneDepts) {
            if(dept.getId()!=null){
                if(oneDept.getId().equals(dept.getId())){
                    oneDept.setDisabled(true);
                }
            }
        }
        return oneDepts;
//        //查询所有的一级部门和二级部门
//        List<Department> departments = mapper.findOneAndTwoList();
//        //遍历集合
//        for (Department department : departments) {
//            //遍历出每一个部门的子部门集合
//            List<Department> children = department.getChildren();
//            if(children.size() == 0){
//                department.setChildren(null);
//            }
//            //递归获取子集目录
//            findeptsTree(children);
//        }
//        return departments;
    }
    @Override
    public void save(Department dept){
        if(dept.getId()==null){
            mapper.add(dept);
        }else{
            mapper.update(dept);
        }
    }
}
