package cn.itsource.pethome.org.service;

import cn.itsource.pethome.basic.service.IBaseService;
import cn.itsource.pethome.basic.util.AjaxResult;
import cn.itsource.pethome.org.domain.Department;
import cn.itsource.pethome.org.domain.Shop;

import java.util.List;

/**
 * 业务层
 */
public interface IShopService extends IBaseService<Shop>{
    //店铺入驻业务层
    void settlement(Shop shop);
}
