package cn.itsource.pethome.org.service;

import cn.itsource.pethome.basic.service.IBaseService;
import cn.itsource.pethome.org.domain.DictionaryDetail;
import cn.itsource.pethome.org.domain.DictionaryType;

/**
 * 数据字典类型业务层
 */
public interface IDictionaryTypeService extends IBaseService<DictionaryType>{
}
