package cn.itsource.pethome.org.service;

import cn.itsource.pethome.basic.service.IBaseService;
import cn.itsource.pethome.org.domain.Employee;

import java.util.List;

public interface IEmployeeService extends IBaseService<Employee>{
    /**
     * 查询出所有的部门经理
     * @return
     */
    List<Employee> findManagers();

}
