package cn.itsource.pethome.org.service.impl;

import cn.itsource.pethome.basic.service.impl.BaseServiceImpl;
import cn.itsource.pethome.org.domain.DictionaryDetail;
import cn.itsource.pethome.org.domain.DictionaryType;
import cn.itsource.pethome.org.mapper.DictionaryDetailMapper;
import cn.itsource.pethome.org.mapper.DictionaryTypeMapper;
import cn.itsource.pethome.org.service.IDictionaryDetailService;
import cn.itsource.pethome.org.service.IDictionaryTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 数据字典明细实现类
 */
@Service
public class DictionaryTypeServiceImpl extends BaseServiceImpl<DictionaryType> implements IDictionaryTypeService {
    @Autowired
    DictionaryTypeMapper mapper;
    @Override
    public void save(DictionaryType type){
        if(type.getId()==null){
            mapper.add(type);
        }else{
            mapper.update(type);
        }
    }
}

