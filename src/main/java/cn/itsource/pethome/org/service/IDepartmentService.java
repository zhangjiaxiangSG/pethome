package cn.itsource.pethome.org.service;

import cn.itsource.pethome.basic.service.IBaseService;
import cn.itsource.pethome.org.domain.Department;

import java.util.List;

/**
 * 部门业务层
 */
public interface IDepartmentService  extends IBaseService<Department>{
    /**
     * 查询所有的上级部门树
     * @return
     */
    List<Department> findDeptsTree(Department dept);
}
