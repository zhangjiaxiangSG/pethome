package cn.itsource.pethome.org.mapper;

import cn.itsource.pethome.basic.mapper.BaseMapper;
import cn.itsource.pethome.org.domain.DictionaryType;
import cn.itsource.pethome.org.domain.Shop;

import java.util.List;

/**
 * 店铺管理mapper
 */
public interface ShopMapper extends BaseMapper<Shop> {
    //查询所有店铺
    List<Shop> findAll();
}
