package cn.itsource.pethome.org.mapper;

import cn.itsource.pethome.basic.mapper.BaseMapper;
import cn.itsource.pethome.org.domain.Department;
import cn.itsource.pethome.org.domain.DictionaryDetail;

/**
 * 数据字典类型接口
 */
public interface DictionaryDetailMapper extends BaseMapper<DictionaryDetail> {
    void deleteBytid(Long tid);
}
