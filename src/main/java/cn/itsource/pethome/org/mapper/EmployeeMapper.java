package cn.itsource.pethome.org.mapper;

import cn.itsource.pethome.basic.mapper.BaseMapper;
import cn.itsource.pethome.org.domain.Employee;

import java.util.List;

public interface EmployeeMapper extends BaseMapper<Employee>{
    /**
     * 查询出所有部门经理
     * @return
     */
    List<Employee> findManagers();

    /**
     * 根据lodinid查询信息
     * @param id
     * @return
     */
    Employee findByLoginId(Long id);
}
