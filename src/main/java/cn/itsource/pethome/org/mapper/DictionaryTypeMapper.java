package cn.itsource.pethome.org.mapper;

import cn.itsource.pethome.basic.mapper.BaseMapper;
import cn.itsource.pethome.org.domain.DictionaryDetail;
import cn.itsource.pethome.org.domain.DictionaryType;

/**
 * 数据字典类型mapper
 */
public interface DictionaryTypeMapper extends BaseMapper<DictionaryType> {
}
