package cn.itsource.pethome.org.mapper;

import cn.itsource.pethome.basic.mapper.BaseMapper;
import cn.itsource.pethome.org.domain.Department;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 部门mapper层
 */
@Component
public interface DepartmentMapper extends BaseMapper<Department>{
    /**
     * 查询部门所有的一级部门和二级部门
     * @return
     */
    List<Department> findOneAndTwoList();

    /**
     * 查询所有的一级部门
     * @return
     */
    List<Department> findOneDepts();

    /**
     * 通过父id查询出子部门
     * @param id
     * @return
     */
    List<Department> findByPid(Long id);
}
