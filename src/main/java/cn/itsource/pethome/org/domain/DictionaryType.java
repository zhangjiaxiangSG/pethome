package cn.itsource.pethome.org.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DictionaryType {

    private Long id;
    /**
     * 类型描述
     */
    private String sn;

    /**
     * 类型名称
     */
    private String name;
    /**
     *
     */
    private List<DictionaryDetail> child;
}
