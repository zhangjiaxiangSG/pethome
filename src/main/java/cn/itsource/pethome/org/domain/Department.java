package cn.itsource.pethome.org.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Department {
    /**
     *部门id
     */
    private Long id;
    /**
     * 部门编号
     */
    private String sn;

    /**
     * 部门名
     */
    private String name;
    /**
     * 部门状态
     */
    private Long state;
    /**
     * 部门经理
     */
    private Employee manager;
    /**
     * 上级部门对象
     */
    private Department parent;

    /**
     * 父路径
     */
    private String dirPath;
    /**
     * 表示所有的子部门
     */
    private List<Department> children;
    private boolean disabled;
    /**
     * 因为前台的级联菜单需要有vulue和label对应的值，所以需要设置属性
     */
    public String getValue(){
        return this.id.toString();
    }
    public String getLabel(){
        return this.name;
    }
}
