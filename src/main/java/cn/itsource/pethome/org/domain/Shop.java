package cn.itsource.pethome.org.domain;

import cn.itsource.pethome.basic.domain.BaseDomain;
import lombok.Data;

import java.util.Date;

@Data
public class Shop{
    private Long id;
    //店铺名
    private String name;
    //店铺电话号码
    private String tel;
    //注册时间
    private Date registerTime = new Date();
    //状态  0 待审核   1 待激活   2正常    -1 驳回
    private Integer state = 0;
    //店铺地址
    private String address;
    //店铺logo
    private String logo;
    //店长(店铺管理人员)
    private Employee admin;
}
