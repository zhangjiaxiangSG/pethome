package cn.itsource.pethome.org.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DictionaryDetail {
    private Long id;
    /**
     * 名称
     */
    private String name;

    /**
     * 父类型id
     */
    private Long types_id;
    /**
     * 数据类型
     */
    private DictionaryType parent;
}
