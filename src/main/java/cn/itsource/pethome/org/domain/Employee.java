package cn.itsource.pethome.org.domain;

import cn.itsource.pethome.user.domain.LoginInfo;
import lombok.Data;

@Data
public class Employee {
    private Long id;
    /**
     * 用户名
     */
    private String username;
    //email
    private String email;
    //电话
    private String phone;
    //盐值
    private String salt;
    //密码
    private String password;
    //年龄
    private String age;
    //状态
    private String state;
    //所属部门
    private Department department;
    //所属店铺
    private Shop shop;
    private LoginInfo login;
}

