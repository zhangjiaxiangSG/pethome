package cn.itsource.pethome.org.controller;

import cn.itsource.pethome.org.domain.Department;
import cn.itsource.pethome.org.domain.Employee;
import cn.itsource.pethome.org.query.DeptQuery;
import cn.itsource.pethome.org.service.IDepartmentService;
import cn.itsource.pethome.org.service.IEmployeeService;
import cn.itsource.pethome.basic.util.PageInfo;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/department")
@Api(tags = "部门管理所有的接口",description = "部门接口详细描述")
public class DepartmentController {
    @Autowired
    IDepartmentService service;
    @Autowired
    IEmployeeService EmployeeService;
    @PatchMapping
    public PageInfo<Department> findPageByQuery(@RequestBody DeptQuery query){
        return service.findPageByQuery(query);
    }
    @GetMapping("/{id}")// 相当于      @RequestMapping(value = "/department/{id}",method = RequestMethod.GET)
    public Department findById(@PathVariable("id") Long id){
        return service.findById(id);
    }
    @DeleteMapping("/{id}")
    public Map<String,Object> delete(@PathVariable("id") Long id){
        Map<String, Object> map = new HashMap<>();
        try {
            service.delete(id);
            map.put("success",true);
        } catch (Exception e) {
            map.put("success",false);
            map.put("msg","删除失败");
        }
        return map;
    }
    @PostMapping
    public Map<String,Object> save(@RequestBody Department dept){
        Map<String, Object> map = new HashMap<>();
        try {
            service.save(dept);
            map.put("success",true);
        } catch (Exception e) {
            map.put("success",false);
            map.put("msg","保存失败");
        }
        return map;
    }
    @PatchMapping("/batchRemove")
    public Map<String,Object> batchRemove(@RequestBody List<Department> list){
        Map<String, Object> map = new HashMap<>();
        try {
            service.batchRemove(list);
            map.put("success",true);
        } catch (Exception e) {
            map.put("success",false);
            map.put("msg","删除失败");
        }
        return map;
    }
    @PatchMapping("/findManagers")
    public List<Employee> findManagers(){
       return EmployeeService.findManagers();
    }
    @PatchMapping("/findDeptsTree")
    public List<Department> findDeptsTree(@RequestBody Department dept){
       return service.findDeptsTree(dept);
    }
}
