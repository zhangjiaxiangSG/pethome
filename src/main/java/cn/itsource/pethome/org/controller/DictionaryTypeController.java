package cn.itsource.pethome.org.controller;

import cn.itsource.pethome.basic.util.PageInfo;
import cn.itsource.pethome.org.domain.DictionaryType;
import cn.itsource.pethome.org.domain.DictionaryType;
import cn.itsource.pethome.org.domain.Employee;
import cn.itsource.pethome.org.query.DeptQuery;
import cn.itsource.pethome.org.query.DictionaryTypeQuery;
import cn.itsource.pethome.org.service.IDictionaryDetailService;
import cn.itsource.pethome.org.service.IDictionaryTypeService;
import cn.itsource.pethome.org.service.IDictionaryTypeService;
import cn.itsource.pethome.org.service.IEmployeeService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/DictionaryType")
@Api(tags = "数据字典所有的接口",description = "数据字典接口详细描述")
public class DictionaryTypeController {
    @Autowired
    IDictionaryTypeService service;
    @Autowired
    IDictionaryDetailService DetailService;
    @PatchMapping
    public PageInfo<DictionaryType> findPageByQuery(@RequestBody DictionaryTypeQuery query){
        return service.findPageByQuery(query);
    }
    @GetMapping("/{id}")// 相当于      @RequestMapping(value = "/department/{id}",method = RequestMethod.GET)
    public DictionaryType findById(@PathVariable("id") Long id){
        return service.findById(id);
    }
    @DeleteMapping("/{id}")
    public Map<String,Object> delete(@PathVariable("id") Long id){
        Map<String, Object> map = new HashMap<>();
        try {
            //删除当前数据字典的明细,先删除外键的数据
            DetailService.deleteBytid(id);
            service.delete(id);
            map.put("success",true);
        } catch (Exception e) {
            map.put("success",false);
            map.put("msg","删除失败");
        }
        return map;
    }
    @PostMapping
    public Map<String,Object> save(@RequestBody DictionaryType type){
        Map<String, Object> map = new HashMap<>();
        try {
            service.save(type);
            map.put("success",true);
        } catch (Exception e) {
            map.put("success",false);
            map.put("msg","保存失败");
        }
        return map;
    }
    @PatchMapping("/batchRemove")
    public Map<String,Object> batchRemove(@RequestBody List<DictionaryType> list){
        Map<String, Object> map = new HashMap<>();
        try {
            //删除类型对应的明细
            for(DictionaryType type : list){
                if(type.getId()!=null){
                    //删除当前数据字典的明细
                    DetailService.deleteBytid(type.getId());
                }
            }
            service.batchRemove(list);
            map.put("success",true);
        } catch (Exception e) {
            map.put("success",false);
            map.put("msg","删除失败");
        }
        return map;
    }
}
