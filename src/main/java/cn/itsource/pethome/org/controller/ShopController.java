package cn.itsource.pethome.org.controller;

import cn.itsource.pethome.basic.util.AjaxResult;
import cn.itsource.pethome.basic.util.PageInfo;
import cn.itsource.pethome.org.domain.Department;
import cn.itsource.pethome.org.domain.Shop;
import cn.itsource.pethome.org.query.DeptQuery;
import cn.itsource.pethome.org.service.IDepartmentService;
import cn.itsource.pethome.org.service.IShopService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/shop")
@Api(tags = "店铺管理所有的接口",description = "店铺接口详细描述")
public class ShopController {
    @Autowired
    IShopService service;
    @PostMapping("/settlement")
    public AjaxResult save(@RequestBody Shop shop){
        try {
            service.settlement(shop);
            return new AjaxResult();
        } catch (Exception e) {
            return new AjaxResult(false,e.getMessage());
        }
    }
}
