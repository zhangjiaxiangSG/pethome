package cn.itsource.pethome.org.controller;

import cn.itsource.pethome.basic.util.PageInfo;
import cn.itsource.pethome.org.domain.DictionaryDetail;
import cn.itsource.pethome.org.domain.Employee;
import cn.itsource.pethome.org.query.DeptQuery;
import cn.itsource.pethome.org.query.DictionaryDetailQuery;
import cn.itsource.pethome.org.service.IDictionaryDetailService;
import cn.itsource.pethome.org.service.IEmployeeService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/DictionaryDetail")
@Api(tags = "数据字典明细所有的接口",description = "数据字典明细详细描述")
public class DictionaryDetailController {
    @Autowired
    IDictionaryDetailService service;
    @PatchMapping
    public PageInfo<DictionaryDetail> findPageByQuery(@RequestBody DictionaryDetailQuery query){
        return service.findPageByQuery(query);
    }
    @GetMapping("/{id}")// 相当于      @RequestMapping(value = "/department/{id}",method = RequestMethod.GET)
    public DictionaryDetail findById(@PathVariable("id") Long id){
        return service.findById(id);
    }
    @DeleteMapping("/{id}")
    public Map<String,Object> delete(@PathVariable("id") Long id){
        Map<String, Object> map = new HashMap<>();
        try {
            service.delete(id);
            map.put("success",true);
        } catch (Exception e) {
            map.put("success",false);
            map.put("msg","删除失败");
        }
        return map;
    }
    @PostMapping
    public Map<String,Object> save(@RequestBody DictionaryDetail detail){
        Map<String, Object> map = new HashMap<>();
        try {
            service.save(detail);
            map.put("success",true);
        } catch (Exception e) {
            map.put("success",false);
            map.put("msg","保存失败");
        }
        return map;
    }
    @PatchMapping("/batchRemove")
    public Map<String,Object> batchRemove(@RequestBody List<DictionaryDetail> list){
        Map<String, Object> map = new HashMap<>();
        try {
            service.batchRemove(list);
            map.put("success",true);
        } catch (Exception e) {
            map.put("success",false);
            map.put("msg","删除失败");
        }
        return map;
    }
}
