package cn.itsource.pethome.org.query;

import cn.itsource.pethome.basic.query.BaseQuery;
import lombok.Data;

@Data
public class DeptQuery extends BaseQuery {
    /**
     * 要查询的状态
     */
    private Long state;
    /**
     * 要查询的部门名
     */
    private String name;
}
