package cn.itsource.pethome.order.service.impl;

import cn.itsource.pethome.basic.service.impl.BaseServiceImpl;
import cn.itsource.pethome.basic.util.CodeGenerateUtils;
import cn.itsource.pethome.basic.util.ContanstUtil;
import cn.itsource.pethome.order.domain.AdoptOrder;
import cn.itsource.pethome.order.domain.OrderAddress;
import cn.itsource.pethome.order.mapper.AdoptOrderMapper;
import cn.itsource.pethome.order.mapper.OrderAddressMapper;
import cn.itsource.pethome.order.mapper.UserAddressMapper;
import cn.itsource.pethome.order.service.IAdoptOrderService;
import cn.itsource.pethome.org.domain.Employee;
import cn.itsource.pethome.org.domain.Shop;
import cn.itsource.pethome.org.mapper.ShopMapper;
import cn.itsource.pethome.pet.domain.Pet;
import cn.itsource.pethome.pet.mapper.PetMapper;
import cn.itsource.pethome.user.domain.LoginInfo;
import cn.itsource.pethome.user.domain.User;
import cn.itsource.pethome.user.domain.UserAddress;
import cn.itsource.pethome.user.mapper.UserMapper;
import org.apache.commons.lang3.time.DateUtils;
import org.assertj.core.util.DateUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;
@Service
public class AdoptOrderServiceImpl extends BaseServiceImpl<AdoptOrder> implements IAdoptOrderService {
    @Autowired
    PetMapper petMapper;
    @Autowired
    UserMapper userMapper;
    @Autowired
    ShopMapper shopMapper;
    //用户地址mapper
    @Autowired
    private UserAddressMapper userAddressMapper;
    //订单地址mapper
    @Autowired
    private OrderAddressMapper orderAddressMapper;
    @Autowired
    AdoptOrderMapper adoptOrderMapper;
    @Override
    @Transactional
    public void submitOrder(Map<String, Object> map, LoginInfo loginInfo) {
        //用户地址id
        Long addressId = Long.valueOf(map.get("addressId").toString()) ;
        //宠物id
        Long petId = Long.valueOf(map.get("petId").toString());
        //支付方式
        Integer payType = (Integer) map.get("payType");
        //通过宠物id获取宠物
        Pet pet = petMapper.findById(petId);
        //设置下架状态
        pet.setState(ContanstUtil.activated);
        //设置下架时间
        pet.setOffsaletime(new Date());
        //设置所属人
        Long loginInfoId = loginInfo.getId();
        User oneByLoginInfoId = userMapper.findOneByLoginInfoId(loginInfoId);
        pet.setUser(oneByLoginInfoId);
        //更新宠物对象
        petMapper.update(pet);
        //创建地址订单
        //通过地址id查询出地址对象
        UserAddress userAddress = userAddressMapper.findById(addressId);
        AdoptOrder order = createAdoptOrder(pet,oneByLoginInfoId);
        //添加订单到数据库
        adoptOrderMapper.add(order);
        String orderSn = order.getOrderSn();
        //创建地址订单
        OrderAddress orderAddress = createAddressOrder(userAddress,orderSn);
        //添加地址订单到数据库
        orderAddressMapper.add(orderAddress);
    }

    /**
     * 创建地址订单
     * @param pet
     * @param loginInfo
     * @return
     */
    private AdoptOrder createAdoptOrder(Pet pet, User loginInfo) {
        AdoptOrder adoptOrder = new AdoptOrder();
        //设置摘要
        adoptOrder.setDigest("[摘要]对"+pet.getName()+"的订单");
        //设置价格
        adoptOrder.setPrice(pet.getSaleprice());
        //设置订单编号
        adoptOrder.setOrderSn(CodeGenerateUtils.generateOrderSn(loginInfo.getId()));
        //设置最后支付时间
        adoptOrder.setLastConfirmTime(DateUtils.addMinutes(new Date(), 15));
        //设置宠物
        adoptOrder.setPet(pet);
        //设置用户
        adoptOrder.setUser(loginInfo);
        //设置店铺
        adoptOrder.setShop(pet.getShop());
        return adoptOrder;
    }

    /**
     * 创建地址订单
     * @param userAddress
     * @return
     */
    private OrderAddress createAddressOrder(UserAddress userAddress,String orderSn) {
        OrderAddress orderAddress = new OrderAddress();
        BeanUtils.copyProperties(userAddress, orderAddress);
        orderAddress.setCreateTime(new Date());
        orderAddress.setOrderSn(orderSn);
        return orderAddress;
    }
}
