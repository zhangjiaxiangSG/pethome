package cn.itsource.pethome.order.service;

import cn.itsource.pethome.basic.service.IBaseService;
import cn.itsource.pethome.order.domain.ProductOrder;
import cn.itsource.pethome.user.domain.LoginInfo;

import java.util.Map;

public interface IProductOrderService extends IBaseService<ProductOrder>{
    /**
     * 提交服务订单
     * @param map
     * @param loginInfo
     */
    void submitOrder(Map<String, Object> map, LoginInfo loginInfo);
}
