package cn.itsource.pethome.order.service.impl;

import cn.itsource.pethome.basic.service.impl.BaseServiceImpl;
import cn.itsource.pethome.basic.util.CodeGenerateUtils;
import cn.itsource.pethome.basic.util.ContanstUtil;
import cn.itsource.pethome.basic.util.DistanceUtil;
import cn.itsource.pethome.order.domain.OrderAddress;
import cn.itsource.pethome.order.domain.ProductOrder;
import cn.itsource.pethome.order.domain.ProductOrderDetail;
import cn.itsource.pethome.order.mapper.OrderAddressMapper;
import cn.itsource.pethome.order.mapper.ProductOrderDetailMapper;
import cn.itsource.pethome.order.mapper.ProductOrderMapper;
import cn.itsource.pethome.order.mapper.UserAddressMapper;
import cn.itsource.pethome.order.service.IProductOrderService;
import cn.itsource.pethome.org.domain.Shop;
import cn.itsource.pethome.org.mapper.ShopMapper;
import cn.itsource.pethome.product.domain.Product;
import cn.itsource.pethome.product.mapper.ProductMapper;
import cn.itsource.pethome.user.domain.LoginInfo;
import cn.itsource.pethome.user.domain.User;
import cn.itsource.pethome.user.domain.UserAddress;
import cn.itsource.pethome.user.mapper.UserMapper;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
@Service
public class ProductOrderServiceImpl extends BaseServiceImpl<ProductOrder> implements IProductOrderService {
    @Autowired
    ProductOrderMapper productOrderMapper;
    @Autowired
    ShopMapper shopMapper;
    @Autowired
    UserMapper userMapper;
    @Autowired
    UserAddressMapper userAddressMapper;
    @Autowired
    OrderAddressMapper orderAddressMapper;
    @Autowired
    ProductMapper productMapper;
    @Autowired
    ProductOrderDetailMapper productOrderDetailMapper;
    @Override
    @Transactional
    public void submitOrder(Map<String, Object> map, LoginInfo loginInfo) {
        //获取产品信息
        List<Map<String,Integer>> products = (List<Map<String,Integer>>)map.get("products");
        //用户地址id
        Long addressId = Long.valueOf(map.get("addressId").toString());
        //支付方式
        Integer payType = Integer.valueOf(map.get("payType").toString());
        //获取用户信息
        User user = userMapper.findOneByLoginInfoId(loginInfo.getId());
        //通过地址id获取地址信息
        UserAddress userAddress = userAddressMapper.findById(addressId);
        //创建产品订单对象
        ProductOrder productOrder = createProductOrder(products,user);
        //设置产品订单的最近的店铺
        Shop nearestShop = DistanceUtil.getNearestShop(DistanceUtil.getPoint(userAddress.getFullAddress()), shopMapper.findAll());
        productOrder.setShop(nearestShop);
        //添加订单信息到数据库##
        productOrderMapper.add(productOrder);
        //得到产品订单的编号
        String orderSn = productOrder.getOrderSn();
        //创建地址订单
        OrderAddress addressOrder = createAddressOrder(userAddress, orderSn);
        orderAddressMapper.add(addressOrder);
        //创建产品详情订单
        List<ProductOrderDetail> productOrderDetails = createProductOrderDetail(productOrder,products);
        //批量添加
        productOrderDetailMapper.batchUpdate(productOrderDetails);
    }

    /**
     * 创建产品详情订单
     * @param productOrder
     * @return
     */
    //服务产品
    private Product product;
    //购买服务数量
    private Integer saleCount;
    //服务订单
    private ProductOrder order;
    private List<ProductOrderDetail> createProductOrderDetail(ProductOrder productOrder,List<Map<String,Integer>> products) {
        ArrayList<ProductOrderDetail> productOrderDetails = new ArrayList<>();
        //遍历map
        for (Map<String, Integer> product : products) {
            //得到map的参数
            Integer id = product.get("id");
            Integer num = product.get("num");
            //根据id查询出所有信息
            Product byId = productMapper.findById(Long.valueOf(id));
            ProductOrderDetail productOrderDetail = new ProductOrderDetail();
            //为新产品订单详情对象设置值
           // productOrderDetail.setCreateTime(new Date());
            productOrderDetail.setProduct(byId);
            productOrderDetail.setSaleCount(num);
            productOrderDetail.setOrder(productOrder);
            //把产品详情对象添加到集合中
            productOrderDetails.add(productOrderDetail);
        }
        return productOrderDetails;
    }

    /**
     * 创建地址订单
     * @param userAddress
     * @return
     */
    private OrderAddress createAddressOrder(UserAddress userAddress,String orderSn) {
        OrderAddress orderAddress = new OrderAddress();
        BeanUtils.copyProperties(userAddress, orderAddress);
        orderAddress.setCreateTime(new Date());
        orderAddress.setOrderSn(orderSn);
        return orderAddress;
    }
    /**
     * 创建服务订单
     * @param products
     * @param user
     * @return
     */
    private ProductOrder createProductOrder(List<Map<String, Integer>> products, User user) {
        ProductOrder productOrder = new ProductOrder();
        //总价格
        BigDecimal totalPrice = new BigDecimal("0");
        StringBuilder sb = new StringBuilder();
        for (int i=0;i<products.size();i++) {
            Map<String, Integer> product = products.get(i);
            //获取产品的id
            Integer id = product.get("id");
            Integer num = product.get("num");
            //通过id查询当前产品的所有信息
            Product byId = productMapper.findById(Long.valueOf(id));
            //价格=数量*售价
            BigDecimal multiply = new BigDecimal(num.toString()).multiply(byId.getSaleprice());
            //总价格累加
            totalPrice = totalPrice.add(multiply);
            if (i!=products.size()){
                sb.append(byId.getName()+",");
            }else{
                sb.append(byId.getName());
            }

        }
        //设置订单摘要
        productOrder.setDigest("[摘要]对"+sb+"的订单摘要");
        //设置订单状态
        productOrder.setState(ContanstUtil.REIMBURSEMENT);
        //设置价格
        productOrder.setPrice(totalPrice);
        //设置订单编号
        productOrder.setOrderSn(CodeGenerateUtils.generateOrderSn(user.getId()));
        //设置最后支付时间 15分钟
        productOrder.setLastPayTime(DateUtils.addMinutes(new Date(), 15));
        //设置服务所属用户
        productOrder.setUser(user);
        return productOrder;
    }
}
