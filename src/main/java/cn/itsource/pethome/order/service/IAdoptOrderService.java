package cn.itsource.pethome.order.service;

import cn.itsource.pethome.basic.service.IBaseService;
import cn.itsource.pethome.order.domain.AdoptOrder;
import cn.itsource.pethome.user.domain.LoginInfo;

import java.util.Map;

public interface IAdoptOrderService extends IBaseService<AdoptOrder>{
    //提交订单
    void submitOrder(Map<String, Object> map, LoginInfo loginInfo);
}
