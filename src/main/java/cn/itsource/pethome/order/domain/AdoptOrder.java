package cn.itsource.pethome.order.domain;

import cn.itsource.pethome.basic.domain.BaseDomain;
import cn.itsource.pethome.org.domain.Shop;
import cn.itsource.pethome.pet.domain.Pet;
import cn.itsource.pethome.user.domain.User;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 领养订单
 */
@Data
public class AdoptOrder extends BaseDomain {
    //摘要
    private String digest;
    //状态   1 待支付  2.待确认  3已完成   -1 已取消
    private Integer state = 1;
    //价格
    private BigDecimal price;
    //订单编号
    private String orderSn;
    //支付单号（支付宝那边支付成功之后，会返回一个支付单号，所以这里不用管）
    private String paySn;
    //最后支付时间
    private Date lastPayTime;
    //支付成功之后的完成时间
    private Date lastConfirmTime;
    //宠物
    private Pet pet;
    //用户
    private User user;
    //店铺
    private Shop shop;
}
