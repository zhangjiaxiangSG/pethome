package cn.itsource.pethome.order.domain;

import cn.itsource.pethome.basic.domain.BaseDomain;
import cn.itsource.pethome.product.domain.Product;
import lombok.Data;

import java.util.Date;

@Data
public class ProductOrderDetail extends BaseDomain {
    //创建时间
    private Date createTime = new Date();
    //服务产品
    private Product product;
    //购买服务数量
    private Integer saleCount;
    //服务订单
    private ProductOrder order;


}
