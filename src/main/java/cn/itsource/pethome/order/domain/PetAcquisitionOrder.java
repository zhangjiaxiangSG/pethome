package cn.itsource.pethome.order.domain;

import cn.itsource.pethome.basic.domain.BaseDomain;
import cn.itsource.pethome.org.domain.Employee;
import cn.itsource.pethome.org.domain.Shop;
import cn.itsource.pethome.pet.domain.Pet;
import cn.itsource.pethome.user.domain.User;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 宠物收购订单
 */
@Data
public class PetAcquisitionOrder extends BaseDomain {
    //摘要
    private String digest;
    //1 待报账(垫付)  2 待打款(银行转账)  3 完成   -1 取消
    private Integer state;
    //记录订单完成时间
    private Date lastcomfirmtime;
    //收购价格
    private BigDecimal price;
    //收购地址
	private String address;
	//订单编号
    private String orderSn;
    //支付单号(垫付:支付宝或者微信产生的支付单号     银行转账: 银行的转账流水号)
    private String paySn;
    //宠物
    private Pet pet;
    //信息发布者
    private User user;
    //支付类型  1垫付  2银行卡转账
    private Integer paytype;
    //收购订单对应的店铺
    private Shop shop;
    //员工
    private Employee employee;

}
