package cn.itsource.pethome.order.mapper;

import cn.itsource.pethome.basic.mapper.BaseMapper;
import cn.itsource.pethome.order.domain.AdoptOrder;

public interface AdoptOrderMapper extends BaseMapper<AdoptOrder>{
}
