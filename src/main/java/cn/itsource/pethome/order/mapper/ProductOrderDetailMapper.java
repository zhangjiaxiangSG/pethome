package cn.itsource.pethome.order.mapper;

import cn.itsource.pethome.basic.mapper.BaseMapper;
import cn.itsource.pethome.order.domain.ProductOrderDetail;

import java.util.List;

public interface ProductOrderDetailMapper extends BaseMapper<ProductOrderDetail>{
    /**
     * 批量添加详情
     * @param productOrderDetails
     */
    void batchUpdate(List<ProductOrderDetail> productOrderDetails);
}
