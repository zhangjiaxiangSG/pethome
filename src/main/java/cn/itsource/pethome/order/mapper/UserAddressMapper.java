package cn.itsource.pethome.order.mapper;

import cn.itsource.pethome.basic.mapper.BaseMapper;
import cn.itsource.pethome.user.domain.UserAddress;

public interface UserAddressMapper extends BaseMapper<UserAddress> {
}
