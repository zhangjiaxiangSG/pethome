package cn.itsource.pethome.order.controller;

import cn.itsource.pethome.basic.util.AjaxResult;
import cn.itsource.pethome.order.service.IAdoptOrderService;
import cn.itsource.pethome.order.service.IProductOrderService;
import cn.itsource.pethome.product.service.IProductService;
import cn.itsource.pethome.user.domain.LoginInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@RequestMapping("/productOrder")
@RestController
public class ProductOrderController {
    @Autowired
    IProductOrderService productOrderService;
    @Autowired
    private RedisTemplate redisTemplate;
    @PostMapping("/submitOrder")
    public AjaxResult submitOrder(@RequestBody Map<String, Object> map, HttpServletRequest request){
        try {
            //获取token值
            String token = request.getHeader("token");
            //获取登录信息对象
            LoginInfo loginInfo = (LoginInfo) redisTemplate.opsForValue().get(token);
            productOrderService.submitOrder(map,loginInfo);
            return new AjaxResult();
        } catch (Exception e) {
            e.printStackTrace();
            return new AjaxResult(false,e.getMessage());
        }
    }
}