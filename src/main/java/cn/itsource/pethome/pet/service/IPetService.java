package cn.itsource.pethome.pet.service;

import cn.itsource.pethome.basic.service.IBaseService;
import cn.itsource.pethome.pet.domain.Pet;
import cn.itsource.pethome.user.domain.LoginInfo;

import java.util.List;

public interface IPetService extends IBaseService<Pet> {
    /**
     * 处理寻主信息
     * @param pet
     * @param loginInfo
     */
    void handleSearchMasterMsg(Pet pet, LoginInfo loginInfo);

    /**
     * 上架信息
     * @param ids
     */
    void onsale(List<Long> ids);
    /**
     * 下架信息
     * @param ids
     */
    void offsale(List<Long> ids);
}
