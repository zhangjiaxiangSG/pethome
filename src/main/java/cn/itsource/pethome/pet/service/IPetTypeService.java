package cn.itsource.pethome.pet.service;


import cn.itsource.pethome.basic.service.IBaseService;
import cn.itsource.pethome.pet.domain.PetType;

public interface IPetTypeService extends IBaseService<PetType> {

}
