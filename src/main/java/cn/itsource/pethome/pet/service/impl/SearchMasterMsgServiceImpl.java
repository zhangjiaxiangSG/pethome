package cn.itsource.pethome.pet.service.impl;

import cn.itsource.pethome.basic.domain.Point;
import cn.itsource.pethome.basic.service.impl.BaseServiceImpl;
import cn.itsource.pethome.basic.util.DistanceUtil;
import cn.itsource.pethome.org.domain.Shop;
import cn.itsource.pethome.org.mapper.ShopMapper;
import cn.itsource.pethome.pet.domain.SearchMasterMsg;
import cn.itsource.pethome.pet.mapper.SearchMasterMsgMapper;
import cn.itsource.pethome.pet.service.ISearchMasterMsgService;
import cn.itsource.pethome.user.domain.LoginInfo;
import cn.itsource.pethome.user.domain.User;
import cn.itsource.pethome.user.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class SearchMasterMsgServiceImpl extends BaseServiceImpl<SearchMasterMsg> implements ISearchMasterMsgService {
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private ShopMapper shopMapper;

    @Override
    @Transactional
    public void publish(SearchMasterMsg searchMasterMsg, LoginInfo loginInfo) {
        //通过登录对象获取宠物主人的信息
       User user = userMapper.findOneByLoginInfoId(loginInfo.getId());
       //设置searchMasterMsg的user的值
        searchMasterMsg.setUser(user);
        //根据地址获取经纬度
        Point point = DistanceUtil.getPoint(searchMasterMsg.getAddress());
        //查询所有的店铺
        List<Shop> shops = shopMapper.findAll();
        //查询当前地址最近的店铺
        Shop nearestShop = DistanceUtil.getNearestShop(point, shops);
        //设置对象所属店铺
        searchMasterMsg.setShop(nearestShop);
        //添加
        //保存寻主信息
        super.add(searchMasterMsg);
    }
}
