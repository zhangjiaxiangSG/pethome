package cn.itsource.pethome.pet.service;

import cn.itsource.pethome.basic.service.IBaseService;
import cn.itsource.pethome.pet.domain.PetDetail;

public interface IPetDetailService extends IBaseService<PetDetail> {
    PetDetail getByPetId(Long petId);
}
