package cn.itsource.pethome.pet.service.impl;

import cn.itsource.pethome.basic.service.impl.BaseServiceImpl;
import cn.itsource.pethome.pet.domain.PetType;
import cn.itsource.pethome.pet.service.IPetTypeService;
import org.springframework.stereotype.Service;

@Service
public class PetTypeServiceImpl extends BaseServiceImpl<PetType> implements IPetTypeService {


}
