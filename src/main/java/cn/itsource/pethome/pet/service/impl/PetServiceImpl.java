package cn.itsource.pethome.pet.service.impl;

import cn.itsource.pethome.basic.service.impl.BaseServiceImpl;
import cn.itsource.pethome.basic.util.CodeGenerateUtils;
import cn.itsource.pethome.basic.util.ContanstUtil;
import cn.itsource.pethome.order.domain.PetAcquisitionOrder;
import cn.itsource.pethome.order.mapper.PetAcquisitionOrderMapper;
import cn.itsource.pethome.org.domain.Employee;
import cn.itsource.pethome.org.domain.Shop;
import cn.itsource.pethome.org.mapper.EmployeeMapper;
import cn.itsource.pethome.pet.domain.Pet;
import cn.itsource.pethome.pet.domain.PetDetail;
import cn.itsource.pethome.pet.domain.SearchMasterMsg;
import cn.itsource.pethome.pet.mapper.PetDetailMapper;
import cn.itsource.pethome.pet.mapper.PetMapper;
import cn.itsource.pethome.pet.mapper.SearchMasterMsgMapper;
import cn.itsource.pethome.pet.service.IPetService;
import cn.itsource.pethome.user.domain.LoginInfo;
import cn.itsource.pethome.user.domain.User;
import cn.itsource.pethome.user.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

@Service
public class PetServiceImpl  extends BaseServiceImpl<Pet> implements IPetService {

    @Autowired
    private PetMapper petMapper;
    @Autowired
    private PetDetailMapper petDetailMapper;
    @Autowired
    private EmployeeMapper employeeMapper;
    @Autowired
    private SearchMasterMsgMapper searchMasterMsgMapper;
    @Autowired
    PetAcquisitionOrderMapper petAcquisitionOrderMapper;
    /**
     * 处理寻主信息
     * @param pet
     * @param loginInfo
     */
    @Override
    @Transactional
    public void handleSearchMasterMsg(Pet pet, LoginInfo loginInfo) {
        //通过logininfo对象获取employee对象
        Employee employee = employeeMapper.findByLoginId(loginInfo.getId());
        //shop更新在pet里面
        pet.setShop(employee.getShop());
        //动态跟新search_master的状态
        //获取寻主表id
        Long id = pet.getSearchMasterMsg().getId();
        SearchMasterMsg searchMasterMsg = searchMasterMsgMapper.findById(id);
        //设置为已处理
        searchMasterMsg.setState(ContanstUtil.OK);
        //更新寻主表的状态
        searchMasterMsgMapper.update(searchMasterMsg);
        //前台信息更新在宠物表里面
        petMapper.add(pet);
        //添加宠物细节表
        PetDetail detail = pet.getDetail();
        //设置宠物
        detail.setPet(pet);
        petDetailMapper.add(detail);
        //重新设置寻主信息对象(msg对象中信息更加齐全一点)
        pet.setSearchMasterMsg(searchMasterMsg);
        //产生收购订单
        PetAcquisitionOrder petAcquisitionOrder = createAcquisitionOrder(pet,employee);
        //跟新在收购表里面
        petAcquisitionOrderMapper.add(petAcquisitionOrder);
    }

    private PetAcquisitionOrder createAcquisitionOrder(Pet pet, Employee loginInfo) {
        //1.创建一个收购订单对象
        PetAcquisitionOrder petAcquisitionOrder = new PetAcquisitionOrder();
        //2.为对象的属性设置值
        //设置摘要
        petAcquisitionOrder.setDigest("[摘要]对"+pet.getName()+"的收购订单");
        //设置地址
        petAcquisitionOrder.setAddress(pet.getSearchMasterMsg().getAddress());
        //设置收购人
        petAcquisitionOrder.setEmployee(loginInfo);
        //设置状态
        //1 待报账(垫付)  2 待打款(银行转账)  3 完成   -1 取消
        petAcquisitionOrder.setState(pet.getPaytype()==1?ContanstUtil.REIMBURSEMENT:ContanstUtil.PAYMENT);
        //收购价格
        petAcquisitionOrder.setPrice(pet.getCostprice());
        //订单编号
        petAcquisitionOrder.setOrderSn(CodeGenerateUtils.generateOrderSn(pet.getSearchMasterMsg().getUser().getId()));
        //宠物
        petAcquisitionOrder.setPet(pet);
        //信息发布者
        petAcquisitionOrder.setUser(pet.getSearchMasterMsg().getUser());
        //设置支付方式
        petAcquisitionOrder.setPaytype(pet.getPaytype());
        //收购订单对应的店铺
        petAcquisitionOrder.setShop(pet.getShop());
        return petAcquisitionOrder;
    }

    @Override
    public void onsale(List<Long> ids) {
        petMapper.onsale(ids);
    }

    @Override
    public void offsale(List<Long> ids) {
        petMapper.offsale(ids);
    }
}