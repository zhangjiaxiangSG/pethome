package cn.itsource.pethome.pet.service.impl;

import cn.itsource.pethome.basic.service.impl.BaseServiceImpl;
import cn.itsource.pethome.pet.domain.PetDetail;
import cn.itsource.pethome.pet.mapper.PetDetailMapper;
import cn.itsource.pethome.pet.service.IPetDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PetDetailServiceImpl extends BaseServiceImpl<PetDetail> implements IPetDetailService {
    @Autowired
    private PetDetailMapper petDetailMapper;
    @Override
    public PetDetail getByPetId(Long petId) {
        return petDetailMapper.loadByPetId(petId);
    }
}
