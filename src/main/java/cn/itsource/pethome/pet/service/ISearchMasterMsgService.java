package cn.itsource.pethome.pet.service;

import cn.itsource.pethome.basic.service.IBaseService;
import cn.itsource.pethome.pet.domain.SearchMasterMsg;
import cn.itsource.pethome.user.domain.LoginInfo;

public interface ISearchMasterMsgService extends IBaseService<SearchMasterMsg> {

    /**
     * 发布寻主信息
     * @param searchMasterMsg 寻主信息对象
     * @param loginInfo  登录信息对象
     */
    void publish(SearchMasterMsg searchMasterMsg, LoginInfo loginInfo);
}
