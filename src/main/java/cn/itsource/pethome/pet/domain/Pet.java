package cn.itsource.pethome.pet.domain;

import cn.itsource.pethome.basic.domain.BaseDomain;
import cn.itsource.pethome.org.domain.Shop;
import cn.itsource.pethome.user.domain.User;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class Pet extends BaseDomain {
    //宠物名(前端传递过来)
    private String name;
    //图片(前端传递过来
    private String resources;
    //销售价(前端传递过来
    private BigDecimal saleprice;
    //后台自动生成
    private Date offsaletime = new Date();
    //上架时间(默认为空)
    private Date onsaletime;
    //状态  0 下架     1 上架
    private Integer state = 0;
    //成本价(前端传递过来)
    private BigDecimal costprice;
    //后台自动生成
    private Date createtime = new Date();
    //宠物类型(前端传递过来)
    private PetType type;
    //后台自动设置进去(登录人对应的店铺)
    private Shop shop;
    //领养人(默认为空)
    private User user;
    //寻主信息对象(前端传递过来)
    private SearchMasterMsg searchMasterMsg;
    //宠物详情(前端传递过来)
    private PetDetail detail = new PetDetail();
    //支付方式，前台传输的
    private Integer paytype;
}
