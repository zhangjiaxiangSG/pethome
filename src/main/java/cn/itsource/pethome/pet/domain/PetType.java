package cn.itsource.pethome.pet.domain;

import cn.itsource.pethome.basic.domain.BaseDomain;
import lombok.Data;

@Data
public class PetType extends BaseDomain {
    private String name;
    private String description;
    private Long pid;

}
