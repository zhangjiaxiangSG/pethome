package cn.itsource.pethome.pet.domain;

import cn.itsource.pethome.basic.domain.BaseDomain;
import cn.itsource.pethome.org.domain.Shop;
import cn.itsource.pethome.user.domain.User;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class SearchMasterMsg extends BaseDomain {
    //宠物名
    private String name;
    //价格
    private BigDecimal price;
    //年龄
    private Integer age;
    //性别
    private Integer gender;
    //毛色
    private String coatColor;
    //图片
    private String resources;
    //宠物类型
    private PetType type;
    //地址
    private String address;
    //寻主信息标题
    private String title;
    //0 待处理       1已处理
    private Integer state = 0;
    //发布人
    private User user;
    //店铺
    private Shop shop;

}
