package cn.itsource.pethome.pet.domain;

import cn.itsource.pethome.basic.domain.BaseDomain;
import lombok.Data;

/**
 * 宠物详情
 */
@Data
public class PetDetail extends BaseDomain {
    //宠物
    private Pet pet;
    //领养须知
    private String adoptNotice;
    //简介
    private String intro;
}
