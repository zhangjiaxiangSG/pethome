package cn.itsource.pethome.pet.mapper;


import cn.itsource.pethome.basic.mapper.BaseMapper;
import cn.itsource.pethome.pet.domain.PetDetail;

import java.io.Serializable;

public interface PetDetailMapper extends BaseMapper<PetDetail> {
    PetDetail loadByPetId(Long productId);

    void removeByPetId(Serializable id);

    void updateByPetId(PetDetail detail);
}
