package cn.itsource.pethome.pet.mapper;


import cn.itsource.pethome.basic.mapper.BaseMapper;
import cn.itsource.pethome.pet.domain.SearchMasterMsg;

import java.util.List;

public interface SearchMasterMsgMapper extends BaseMapper<SearchMasterMsg> {
}
