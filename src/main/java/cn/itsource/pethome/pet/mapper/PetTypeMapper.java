package cn.itsource.pethome.pet.mapper;


import cn.itsource.pethome.basic.mapper.BaseMapper;
import cn.itsource.pethome.pet.domain.PetType;

import java.util.Map;

public interface PetTypeMapper extends BaseMapper<PetType> {
}
