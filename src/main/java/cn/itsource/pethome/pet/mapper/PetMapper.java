package cn.itsource.pethome.pet.mapper;

import cn.itsource.pethome.basic.mapper.BaseMapper;
import cn.itsource.pethome.pet.domain.Pet;

import java.util.List;
import java.util.Map;

public interface PetMapper extends BaseMapper<Pet> {
    /**
     * 宠物上架
     * @param ids
     */
    void onsale(List<Long> ids);

    /**
     * 宠物下架
     * @param ids
     */
    void offsale(List<Long> ids);
}
