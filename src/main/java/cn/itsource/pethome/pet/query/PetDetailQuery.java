package cn.itsource.pethome.pet.query;

import cn.itsource.pethome.basic.query.BaseQuery;
import lombok.Data;

@Data
public class PetDetailQuery extends BaseQuery {
}
