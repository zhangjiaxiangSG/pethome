package cn.itsource.pethome.pet.query;

import cn.itsource.pethome.basic.query.BaseQuery;
import cn.itsource.pethome.org.domain.Shop;
import lombok.Data;

@Data
public class PetQuery extends BaseQuery {
    //0表示下架，1表示上架
    private Integer state;
    private Long shopId;

}
