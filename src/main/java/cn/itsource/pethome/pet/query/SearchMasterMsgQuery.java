package cn.itsource.pethome.pet.query;

import cn.itsource.pethome.basic.query.BaseQuery;
import lombok.Data;

@Data
public class SearchMasterMsgQuery extends BaseQuery {
    //状态   0 待处理       1已处理
    private Integer state;
    //店铺id
    private Long shopId;
}
