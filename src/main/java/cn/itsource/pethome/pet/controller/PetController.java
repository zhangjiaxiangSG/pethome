package cn.itsource.pethome.pet.controller;

import cn.itsource.pethome.basic.util.AjaxResult;
import cn.itsource.pethome.basic.util.PageInfo;
import cn.itsource.pethome.org.domain.Employee;
import cn.itsource.pethome.org.mapper.EmployeeMapper;
import cn.itsource.pethome.org.service.IEmployeeService;
import cn.itsource.pethome.pet.domain.Pet;
import cn.itsource.pethome.pet.domain.PetDetail;
import cn.itsource.pethome.pet.query.PetQuery;
import cn.itsource.pethome.pet.service.IPetService;
import cn.itsource.pethome.product.domain.ProductDetail;
import cn.itsource.pethome.user.domain.LoginInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/pet")
public class PetController {

    @Autowired
    private IPetService petService;
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private EmployeeMapper employeeMapper;

    /**
     * 处理未处理的寻主信息
     * @param pet
     * @param request
     * @return
     */
    @PostMapping
    public AjaxResult handlePet(@RequestBody Pet pet, HttpServletRequest request){
        try {
            //获取token值
            String token = request.getHeader("token");
            //获取登录信息对象
            LoginInfo loginInfo = (LoginInfo) redisTemplate.opsForValue().get(token);
            petService.handleSearchMasterMsg(pet,loginInfo);
            return new AjaxResult();
        } catch (Exception e) {
            e.printStackTrace();
            return new AjaxResult(false,e.getMessage());
        }
    }

    /**
     * 店铺进行上架的业务
     * @param ids
     * @return AjaxResult
     */
    @PostMapping("/onsale")
    public AjaxResult onsale(@RequestBody List<Long> ids){
        try {
            petService.onsale(ids);
            return new AjaxResult();
        } catch (Exception e) {
            e.printStackTrace();
            return new AjaxResult(false,e.getMessage());
        }
    }

    /**
     * 宠物下架
     * @param ids
     * @return
     */
    @PostMapping("/offsale")
    public AjaxResult offsale(@RequestBody List<Long> ids){
        try {
            petService.offsale(ids);
            return new AjaxResult();
        } catch (Exception e) {
            e.printStackTrace();
            return new AjaxResult(false,e.getMessage());
        }
    }
    @PatchMapping
    public PageInfo<Pet> findPageByQuery(@RequestBody PetQuery query, HttpServletRequest request){
        //获取token值
        String token = request.getHeader("token");
        //获取登录信息对象
        LoginInfo loginInfo = (LoginInfo) redisTemplate.opsForValue().get(token);
        //根据登录对象获取employee信息
        Employee employee = employeeMapper.findByLoginId(loginInfo.getId());
        //通过员工查询所在店铺
        query.setShopId(employee.getShop().getId());
        //返回页面信息
        return petService.findPageByQuery(query);
    }
    @PatchMapping("/findHomePageByQuery")
    public PageInfo<Pet> findHomePageByQuery(@RequestBody PetQuery query){
        //返回页面信息
        return petService.findPageByQuery(query);
    }

    /**
     * 通过id得到一条宠物详细信息
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public AjaxResult findoneByPID(@PathVariable("id") Long id){
        try {
            Pet pet = petService.findById(id);
            return new AjaxResult().setResultObj(pet);
        } catch (Exception e) {
            e.printStackTrace();
            return new AjaxResult(false,e.getMessage());
        }
    }
}
