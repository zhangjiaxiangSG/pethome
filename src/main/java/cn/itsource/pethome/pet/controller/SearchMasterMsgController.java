package cn.itsource.pethome.pet.controller;

import cn.itsource.pethome.basic.util.AjaxResult;
import cn.itsource.pethome.basic.util.PageInfo;
import cn.itsource.pethome.org.domain.Employee;
import cn.itsource.pethome.org.mapper.EmployeeMapper;
import cn.itsource.pethome.org.service.IEmployeeService;
import cn.itsource.pethome.pet.domain.SearchMasterMsg;
import cn.itsource.pethome.pet.query.SearchMasterMsgQuery;
import cn.itsource.pethome.pet.service.ISearchMasterMsgService;
import cn.itsource.pethome.user.domain.LoginInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/searchMasterMsg")
public class SearchMasterMsgController {
    @Autowired
    private ISearchMasterMsgService seachMasterMsgService;
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private EmployeeMapper employeeMapper;

    /**
     * 发布寻主信息
     * @param searchMasterMsg
     * @return
     */
    @PostMapping("/publish")
    public AjaxResult publish(@RequestBody SearchMasterMsg searchMasterMsg, HttpServletRequest request){
        try {
            //获取token值
            String token = request.getHeader("token");
            //获取登录信息对象
            LoginInfo loginInfo = (LoginInfo) redisTemplate.opsForValue().get(token);
            //发布寻主信息，信息对象和登录信息对象
            seachMasterMsgService.publish(searchMasterMsg,loginInfo);
            return new AjaxResult();
        } catch (Exception e) {
            e.printStackTrace();
            return new AjaxResult(false, e.getMessage());
        }
    }
    @PatchMapping
    public PageInfo<SearchMasterMsg> findPageByQuery(@RequestBody SearchMasterMsgQuery query, HttpServletRequest request){
            //获取token值
            String token = request.getHeader("token");
            //获取登录信息对象
            LoginInfo loginInfo = (LoginInfo) redisTemplate.opsForValue().get(token);
            Employee employee = employeeMapper.findByLoginId(loginInfo.getId());
            query.setShopId(employee.getShop().getId());
            return seachMasterMsgService.findPageByQuery(query);
    }





}
