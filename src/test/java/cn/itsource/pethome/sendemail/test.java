package cn.itsource.pethome.sendemail;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class test {
    @Autowired
    private JavaMailSender javaMailSender;
    @Test
    public void testSingle(){
        System.out.println(javaMailSender);
        System.out.println(javaMailSender);
    }
    @Test
    //发送简单的邮箱纯文本
    public void testSimpleSender(){
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setFrom("1299352517@qq.com");
        mailMessage.setTo("1299352517@qq.com");
        mailMessage.setSubject("清华大学录取通知书");
        mailMessage.setText("你已被清华大学录取，恭喜你！");
        javaMailSender.send(mailMessage);
    }
    @Test
    //发送复杂的邮箱
    public void testMimeSender() throws MessagingException {
        Send();


    }

    private void Send() throws MessagingException {
        //创建复杂右键对象
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        //创建复杂邮件工具类
        MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true, "utf-8");
        //设置邮件发送方
        mimeMessageHelper.setFrom("1299352517@qq.com");
        //设置邮件接收方
        mimeMessageHelper.setTo("771643148@qq.com");
        //设置邮件标题
        mimeMessageHelper.setSubject("录取通知书");
        //设置邮件的文本
        mimeMessageHelper.setText("<h1>恭喜你加入我们的大家庭</h1>" +
                "<img src='https://www.baidu.com/img/flexible/logo/pc/result@2.png'/>"
                ,true);
        //添加附件
        mimeMessageHelper.addAttachment("1.jpg", new File("C:\\Users\\Administrator\\Pictures\\Saved Pictures\\1.jpg"));
        //设置邮件的附件
        //发送文件
        javaMailSender.send(mimeMessage);
    }
}

