package cn.itsource.pethome.org.service;

import cn.itsource.pethome.App;
import cn.itsource.pethome.org.domain.Department;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = App.class)
public class DepartmentServiceTest {
    @Autowired
    IDepartmentService service;
    @Test
    public void findById() {
        Department byId = service.findById(1L);
        System.out.println(byId);
    }

    @Test
    public void findAll() {
//        List<Department> all = service.findAll(query);
//        for (Department department : all) {
//            System.out.println(department);
//        }
    }

    @Test
    public void save() {
    }

    @Test
    public void delete() {
        service.delete(45l);
    }
}